<?php /*Template Name:Home-page*/?>
<!doctype html>
<html lang="en">
<head><script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.loader');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});
</script>
<meta charset="UTF-8">
<meta charset="UTF-8">
<meta name="description" content="Limo is a valid html5 CSS3 One Page Template and integrated with bootstrap." >
<meta name="keywords" content="CodexCoder,HTML5,CSS3,bootstrap,JavaScript,One Page Template">
<meta name="author" content="CodexCoder">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="all" />
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>><div id="page-preloader">
<div class="loader">
  <div>l</div>
  <div>o</div>
  <div>a</div>
  <div>d</div>
  <div>i</div>
  <div>n</div>
  <div>g</div>
</div>
</div>
<header id="header">
  <section id="headnev" class="navbar topnavbar" >
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
        <a href="#header" class="navbar-brand header"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Responsive Banner for Slider"></a> </div>
      <!-- /.navbar-header -->
      <div class="collapse navbar-collapse">
        <ul class="nav pull-right navbar-nav">
          <li><a class="header" href="#header">Home</a></li>
          <li><a class="worksection" href="#somework">News</a></li>
          <li><a class="service" href="#service">Radio</a></li>
          <li><a class="portfolio" href="#portfolio">TV</a></li>
        </ul>
      </div>
      <!-- /.collapse /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </section>
  <!-- /#headnev -->
</header>
<!-- /#Header-->
<section id="slider">
  <div class="container-full">
    <div class="slider">
      <div id="main-slider" class="carousel slide">
        <ol class="carousel-indicators">
          <li data-target="#main-slider" data-slide-to="0" class="active"></li>
          <li data-target="#main-slider" data-slide-to="1"></li>
          <li data-target="#main-slider" data-slide-to="2"></li>
        </ol>
        <!-- /.carousel-indicators -->
        <!-- Carousel items -->
        <div class="carousel-inner">
          <div class="active item">
            <div class="container slide-element"> <img src="<?php echo get_template_directory_uri(); ?>/img/Responsive-Banner-for-Slider.png" alt="Responsive Banner for Slider">
              <h1>Welcome to Limo</h1>
              <p> Limo is a beautiful HTML5 template Designed and Developed by CodexCoder. 
                This template is validated by W3C and we attached lots of Features. 
                Its awesome outlook and effects can easily attract visitors. </p>
              <p class="readmore"><a class="btn" href="#">Read More</a></p>
            </div>
            <!-- /.slide-element -->
          </div>
          <!--/.active /.item -->
          <div class="item">
            <div class="container slide-element"> <img src="<?php echo get_template_directory_uri(); ?>/img/features.png" alt="Features">
              <h1>Features</h1>
              <p> <br>
                Fully Responsive <br>
                Pricing  Table <br>
                Portfolio <br>
                Services <br>
                About Us <br>
                Team, Contact form, Map and Many More. </p>
              <p class="readmore"><a class="btn" href="#">Read More</a></p>
            </div>
            <!-- /.slide-element -->
          </div>
          <!-- /.item -->
          <div class="item">
            <div class="container slide-element"> <img src="<?php echo get_template_directory_uri(); ?>/img/Used-Component.png" alt="Used Component.png">
              <h1>Used Component </h1>
              <p> Limo is Integrated  With <br>
                Bootstrap 3 <br>
                Font Awesome <br>
                jQuery <br>
                MixitUp </p>
              <p class="readmore"><a class="btn" href="#">Read More</a></p>
            </div>
            <!-- /.slide-element -->
          </div>
          <!-- /.item -->
        </div>
        <!-- /.carousel-inner -->
        <!-- slider nav -->
        <a class="carousel-control left" href="#main-slider" data-slide="prev"><i class="fa fa-chevron-left"></i></a> <a class="carousel-control right" href="#main-slider" data-slide="next"><i class="fa fa-chevron-right"></i></a> </div>
      <!-- /#main-slider -->
    </div>
    <!-- /.slider -->
  </div>
  <!-- /.container-full -->
</section>
<!-- /#Slider -->
<section id="somework">
  <div class="worksection">
    <div class="container-full">
      <div class="works">
        <div class="container">
          <h3>News</h3>
        </div>
      </div>
      <!-- /.works -->
      <div class="container">
        <div class="works-img">
          <div id="work-slide" class="carousel slide">
            <div class="slide2nev"> <a class="carousel-control left" href="#work-slide" data-slide="prev"><i class="fa fa-chevron-left"></i></a> <a class="carousel-control right" href="#work-slide" data-slide="next"><i class="fa fa-chevron-right"></i></a> </div>
            <!-- /.slide2nev -->
            <!-- Slider items -->
            <div class="carousel-inner">
              <div class="active item">
                <?php
$queryObject = new WP_Query( 'post_type=News&posts_per_page=4' );
// The Loop!
//custom postname : videos
if ($queryObject->have_posts()) {
    ?>
                <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        ?>
                <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <div class="col-sm-3">
                  <figure> <img src="<?php echo $src[0];?>" alt="" style="width:255px;height:163px;">
                    <?php /*?><img src="<?php echo get_template_directory_uri(); ?>/img/9.jpg" alt="9"><?php */?>
                    <figcaption>
                      <h4>
                        <?php the_title();?>
                      </h4>
                    </figcaption>
                  </figure>
                  <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
                    <?php the_title();?>
                    </a></h4>
                  <p style="text-align:justify;">
                    <?php $cont = get_the_content(); echo substr($cont,0,155); ?>
                  </p>
                  <div><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" style="text-decoration:none;color:#006633;"><strong>View +</strong></a></div>
                </div>
                <?php
    }
    ?>
                <?php
}
?>
              </div>
              <!-- /.active /.item -->
              <!-- /.item -->
            </div>
            <!-- /.carousel-inner -->
            <!--/.carousel -->
          </div>
          <!-- /#work-slide -->
        </div>
        <!-- /.works-img -->
      </div>
      <!--/.container-->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.worksection -->
</section>
<!-- /#somework -->
<!-- /#aboveportfolio -->
<section id="service">
  <div class="servicediv">
    <div class="container-full">
      <div class="container">
        <h3>Our videos</h3>
        <ul>
          <?php
$queryObject = new WP_Query( 'post_type=Videos&posts_per_page=4' );
// The Loop!
//custom postname : videos
if ($queryObject->have_posts()) {
    ?>
          <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        ?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <li class="col-xs-3">
            <div> <a href=" <?php echo do_shortcode('[types field="add-video-link" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>"> <img src="<?php echo $src[0];?>" alt="" style="width:265px;height:173px;"></a>
              <p class="faicons"> </p>
              <h4>
                <?php the_title();?>
              </h4>
              <p style="text-align:justify;">
                <?php $cont = get_the_content(); echo substr($cont,0,55); ?>
              </p>
            </div>
          </li>
          <?php
    }
    ?>
          <?php
}
?>
        </ul>
      </div>
      <!-- /.container -->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.servicediv -->
</section>
<!-- /#Service -->











<section id="service">
  <div class="servicediv">
    <div class="container-full">
      <div class="container">
        <h3>Radio</h3>
        <ul>
          <?php
$queryObject = new WP_Query( 'post_type=Radio&posts_per_page=4' );
// The Loop!
//custom postname : videos
if ($queryObject->have_posts()) {
    ?>
          <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        ?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <li class="col-xs-3">
            <div> <a href=" <?php echo do_shortcode('[types field="add-radio-links" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>"> <img src="<?php echo $src[0];?>" alt="" style="width:265px;height:173px;"></a>
              <p class="faicons"> </p>
              <h4>
                <?php the_title();?>
              </h4>
              <p style="text-align:justify;">
                <?php $cont = get_the_content(); echo substr($cont,0,55); ?>
              </p>
            </div>
          </li>
          <?php
    }
    ?>
          <?php
}
?>
        </ul>
      </div>
      <!-- /.container -->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.servicediv -->
</section>
<!-- /#Service -->















<section id="portfolio">
  <div class="container-full">
    <div class="container">
      <h2 class="text-center">Our Gallery</h2>
      <p class="text-center"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut laboreet <br>
        olore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea </p>
     
      <!-- /.portfolio-type -->
    </div>
    <!-- /.container -->
    <div class="container">
    
    
    
    
    
      <div class="portfolio-img">
      
      
      
        <ul id="Grid">
        
        <?php
$queryObject = new WP_Query( 'post_type=Gallery&posts_per_page=12' );
// The Loop!
//custom postname : videos
if ($queryObject->have_posts()) {
    ?>
          <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        ?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
        
        
        
        
          <li class="mix category_3" data-cat="3" >
            <figure>
            <img src="<?php echo $src[0];?>" alt="" style="width:265px;height:173px;">
             
              <figcaption>
                <h4><?php the_title();?></h4>
              </figcaption>
            </figure>
          </li>
          
        <?php
    }
    ?>
          <?php
}
?>   
          
          
        </ul>
        <!-- /#Grid -->
      </div>
      <!-- /.portfolio-img -->
    </div>
    <!-- /.container -->
    <div class="clear-bottom"></div>
  </div>
  <!-- /.container-full -->
</section>
<!-- #portfolio -->
<section id="testimonial">
  <div class="testsection">
    <div class="container-full">
      <div class="heading">
        <div class="container">
          <h3 class="text-center">Our Clint testimonial</h3>
        </div>
      </div>
      <!-- /.heading -->
      <div class="container">
      
      
      
        <div class="col-xs-12">
          <?php dynamic_sidebar('Testimonials')?>
          
          
        </div>
        
        
        
      </div>
      <!-- /.container -->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.testsection -->
</section>
<!-- /#testimonial -->

<!-- /#team -->
<section id="contactus">
  <div class="container-full">
    <!-- <div id="map_canvas"></div> /#map_canvas -->
  
    <iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
src=""> </iframe>
    <div class="shadowcontainer">
      <div class="shadow">
        <div class="clear-top"></div>
        <div class="container">
          <div class="col-md-6 pull-left">
           
            
              <?php echo do_shortcode(' [contact-form-7 id="15" title="Contact form 1"]'); ?>
         
           
            
            
          </div>
          <!-- /.col-md-6 -->
          <div class="col-md-3 gettouch pull-right">
            <h4>Get in touch</h4>
            <p class="email"> E-mail </p>
            <p> example@site.com </p>
            <p class="email"> Web </p>
            <p> www.sitess.com </p>
            <p class="address"> Address </p>
            <p> California </p>
          </div>
          <!-- /.col-md-3 /.gettouch -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.shadow -->
    </div>
    <!-- /.shadowcontainer -->
  </div>
  <!-- /.container-full -->
</section>
<!-- /.contactus-->
<section id="scroller">
  <div class="container-full">
    <div class="gototop"></div>
    <div class="gotobottom">
      <div class="goup">
        <div class="gotoup"> <a class="header" href="#header"><i class="fa fa-arrow-circle-up"></i></a> </div>
        <!-- /.gotoup -->
      </div>
      <!-- /.goup -->
    </div>
    <!-- /.gotobottom -->
  </div>
  <!-- /.container-full -->
</section>
<!--   /#scroller  -->
<section id="scroller2">
  <div class="gotop"> <a class="header" href="#header"><i class="fa fa-arrow-circle-o-up"></i></a> </div>
  <!-- /.gotop -->
</section>
<!--   /#scroller2  -->
<section id="footer">
  <footer class="container-full">
    <div class="container">
      <!-- /.col-md-3 -->
    </div>
    <!-- /.container -->
    <div class="clear-bottom copyright">
      <p>&copy; Copyright 2014, All Rights Reserved. Designed and Developed by <a href="http://didikmalaysia.com">didikmalaysia</a></p>
    </div>
    <!-- /.clear-bottom /.copyright -->
  </footer>
  <!-- /.container-full -->
</section>
<!-- /.footer -->
</body>
</html>

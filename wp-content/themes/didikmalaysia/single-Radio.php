<?php /*Template Name:Inner-Pge*/?>
<?php get_header();?>
<section id="testimonial" class="singlepost-section">
  <!-- testimonial start -->
  
  <div class="container">
    <div  style="width:72%;padding:4px;float:left;">
      <h2 style="text-align:left;">
        <?php the_title();?>
      </h2>
      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <?php if(have_posts()) : while(have_posts()) : the_post();?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
         <?php /*?> <img src="<?php echo $src[0];?>" alt="" style="width:790px;height:280px;"><?php */?>
          <p style="color:#D2474D;">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </p>
          <?php the_content(); ?>
          <div class="news-social"> <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&t=<?php the_title();?>" target="blank" class="fb"></a> <a href="http://twitter.com/share?url=<?php the_permalink() ?>&text=<?php the_title();?>" target="blank" class="tw"></a> <a href="https://plus.google.com/share?url=<?php the_permalink() ?>&text=<?php the_title();?>" target="blank" class="gplus"></a> </div>
          <!-- .entry-meta -->
        
          <?php endwhile; endif; ?>
        </div>
        <!-- .entry-content -->
      </div>
    </div>
    <div style="width:25%;float:right;">
      <?php get_sidebar();?>
    </div>
  </div>
</section>
<?php get_footer();?>

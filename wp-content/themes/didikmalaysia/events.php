<?php
/**
 * Template name: Events
 */
?>
<?php get_header(); ?>
<div class="innercontainer event-pg">
<div class="padd">
    <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>
 <h2 style="padding-bottom:20px;"><?php the_title(); ?></h2>
 
<?php /*?><div class="col-2 responsive-ico responsive-event">
	<div class="event-calendar">
		<?php dynamic_sidebar('eventcalendar'); ?>
	</div>
</div> 
 
 

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="padding-right:20px; margin-bottom:15px;">
        <div class="entry-content">
           <?php the_content(); ?>
         </div>
        <!-- .entry-content -->
      </div>
</div><?php */?>
<?php /*?>
<div class="col-2" >
<?php get_sidebar();?>
</div>
<?php */?>


    <div class="col-10">
        <div class="event-calendar">
            <?php dynamic_sidebar('eventcalendar'); ?>
        </div>
    </div>
</div><!--padd-->
</div><!--innercontainer-->





<?php get_footer();?>
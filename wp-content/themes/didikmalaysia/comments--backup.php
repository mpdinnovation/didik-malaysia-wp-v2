<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>

<style type="text/css">



.comment-body img{float:left; padding-right: 10px;}

#comments{padding-bottom:10px;margin-left:-12px;}



ol.commentlist li{ border-bottom: 1px solid #CCCCCC; min-height: 100px; padding-top: 25px;}



ol.commentlist{list-style:none;}

ol.commentlist a{color:#000;}

ol.commentlist .fn{font-style: normal; font-weight: bold; text-transform: uppercase;}

.commentlist .reply a{  background: none repeat scroll 0 0 #CCCCCC; border-radius: 5px 5px 5px 5px; float: right; padding: 5px;color:#FF0000;}

.commentlist .commentmetadata a{font-weight: bold; text-transform: uppercase;}
 .pclass{ margin:1px; padding:5px;}



	.inputclass{ margin:3px; border:1px solid #999; padding:5px; -moz-border-radius:5px; border-radius:4px; margin-bottom:4px;}

	.labelclass{ margin:1px; padding:6px; font-family:"Times New Roman", Times, serif; font-size:14px; font-weight:bold;}



	.submitclass{ padding:5px; margin:5px; font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; background-color:#ccc; -moz-border-radius:5px; border-radius:4px; margin-bottom:4px;}
	
	a.url
	{
	text-align:left !important;
	color:#666666;float:left;padding-right:10px;
	}
	.comment-metadata
	{
	text-align:right;
	color:#666666 !important;
	padding-left:20px;
	}
	
	.comment-metadata a:link
	{
	color:#666666 !important;
	}
    .comment-author vcard
	{
	text-align:left;
	float:left;
	}
	.says
	{
	float:left;
	}
	.comment-reply-link
	{
	float:left;
	color:#F05A29;
	}
	
	.comment-form-author label
	
	{
	padding-right:48px !important; 
	}
	
	.comment-form-email label
	
	{
	padding-right:50px !important; 
	}
	.comment-form-url label
	
	{
	padding-right:46px !important; 
	}
	
	.comment-form-comment label
	
	{
	padding-right:34px !important; 
	}
	
	
	
	label[for=comment]
{
vertical-align:top;
}
	
	
	
	.btn, input[type="submit"] {
    -moz-appearance: none;
    background: none repeat scroll 0 0 transparent;
    border: 3px solid #17b3e6;
    border-radius: 3px;
    box-shadow: none;
    color: #17b3e6;
    cursor: pointer;
    display: inline-block;
    font-size: 13px;
    font-weight: 700;
    margin: 0 0 3px;
    overflow: hidden;
    padding: 13px 24px;
    position: relative;
    text-shadow: none !important;
    text-transform: uppercase;
    transform: translateZ(0px);
    transition: all 0.2s ease-in-out 0s;
}

input[type="text"], input[type="password"], input[type="date"], input[type="datetime"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="time"], input[type="url"], textarea, .woocommerce table.cart td.actions .coupon .input-text {
    border: 1px solid #d7d7d7;
    border-radius: 0;
    box-shadow: none;
    color: gray;
    font-size: 12px;
   /* height: 40px;*/
    margin-bottom: 25px;
    padding: 12px;
    transition: all 0.4s ease-in-out 0s;
}
.comments-title
{
font-size:18px !important;
}
	
</style>



<div class="col-6"  >
<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'twentythirteen' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 74,
				) );
			?>
		</ol><!-- .comment-list -->

		<?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<nav class="navigation comment-navigation" role="navigation">
			<h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'twentythirteen' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'twentythirteen' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'twentythirteen' ) ); ?></div>
		</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , 'twentythirteen' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php comment_form(); ?>
</div>
</div><!-- #comments -->
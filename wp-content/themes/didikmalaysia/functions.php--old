<?php

/**

 * Twenty Thirteen functions and definitions

 *

 * Sets up the theme and provides some helper functions, which are used in the

 * theme as custom template tags. Others are attached to action and filter

 * hooks in WordPress to change core functionality.

 *

 * When using a child theme (see http://codex.wordpress.org/Theme_Development

 * and http://codex.wordpress.org/Child_Themes), you can override certain

 * functions (those wrapped in a function_exists() call) by defining them first

 * in your child theme's functions.php file. The child theme's functions.php

 * file is included before the parent theme's file, so the child theme

 * functions would be used.

 *

 * Functions that are not pluggable (not wrapped in function_exists()) are

 * instead attached to a filter or action hook.

 *

 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API

 *

 * @package WordPress

 * @subpackage Twenty_Thirteen

 * @since Twenty Thirteen 1.0

 */



/*

 * Set up the content width value based on the theme's design.

 *

 * @see twentythirteen_content_width() for template-specific adjustments.

 */

if ( ! isset( $content_width ) )

	$content_width = 604;



/**

 * Add support for a custom header image.

 */

require get_template_directory() . '/inc/custom-header.php';



/**

 * Twenty Thirteen only works in WordPress 3.6 or later.

 */

if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )

	require get_template_directory() . '/inc/back-compat.php';



/**

 * Twenty Thirteen setup.

 *

 * Sets up theme defaults and registers the various WordPress features that

 * Twenty Thirteen supports.

 *

 * @uses load_theme_textdomain() For translation/localization support.

 * @uses add_editor_style() To add Visual Editor stylesheets.

 * @uses add_theme_support() To add support for automatic feed links, post

 * formats, and post thumbnails.

 * @uses register_nav_menu() To add support for a navigation menu.

 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_setup() {

	/*

	 * Makes Twenty Thirteen available for translation.

	 *

	 * Translations can be added to the /languages/ directory.

	 * If you're building a theme based on Twenty Thirteen, use a find and

	 * replace to change 'twentythirteen' to the name of your theme in all

	 * template files.

	 */

	load_theme_textdomain( 'twentythirteen', get_template_directory() . '/languages' );



	/*

	 * This theme styles the visual editor to resemble the theme style,

	 * specifically font, colors, icons, and column width.

	 */

	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', twentythirteen_fonts_url() ) );



	// Adds RSS feed links to <head> for posts and comments.

	add_theme_support( 'automatic-feed-links' );



	/*

	 * Switches default core markup for search form, comment form,

	 * and comments to output valid HTML5.

	 */

	add_theme_support( 'html5', array(

		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'

	) );



	/*

	 * This theme supports all available post formats by default.

	 * See http://codex.wordpress.org/Post_Formats

	 */

	add_theme_support( 'post-formats', array(

		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'

	) );



	// This theme uses wp_nav_menu() in one location.

	register_nav_menu( 'primary', __( 'Navigation Menu', 'twentythirteen' ) );



	/*

	 * This theme uses a custom image size for featured images, displayed on

	 * "standard" posts and pages.

	 */

	add_theme_support( 'post-thumbnails' );

	set_post_thumbnail_size( 604, 270, true );



	// This theme uses its own gallery styles.

	add_filter( 'use_default_gallery_style', '__return_false' );

}

add_action( 'after_setup_theme', 'twentythirteen_setup' );



/**

 * Return the Google font stylesheet URL, if available.

 *

 * The use of Source Sans Pro and Bitter by default is localized. For languages

 * that use characters not supported by the font, the font can be disabled.

 *

 * @since Twenty Thirteen 1.0

 *

 * @return string Font stylesheet or empty string if disabled.

 */

function twentythirteen_fonts_url() {

	$fonts_url = '';



	/* Translators: If there are characters in your language that are not

	 * supported by Source Sans Pro, translate this to 'off'. Do not translate

	 * into your own language.

	 */

	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'twentythirteen' );



	/* Translators: If there are characters in your language that are not

	 * supported by Bitter, translate this to 'off'. Do not translate into your

	 * own language.

	 */

	$bitter = _x( 'on', 'Bitter font: on or off', 'twentythirteen' );



	if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {

		$font_families = array();



		if ( 'off' !== $source_sans_pro )

			$font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';



		if ( 'off' !== $bitter )

			$font_families[] = 'Bitter:400,700';



		$query_args = array(

			'family' => urlencode( implode( '|', $font_families ) ),

			'subset' => urlencode( 'latin,latin-ext' ),

		);

		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );

	}



	return $fonts_url;

}



/**

 * Enqueue scripts and styles for the front end.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_scripts_styles() {

	/*

	 * Adds JavaScript to pages with the comment form to support

	 * sites with threaded comments (when in use).

	 */

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )

		wp_enqueue_script( 'comment-reply' );



	// Adds Masonry to handle vertical alignment of footer widgets.

	if ( is_active_sidebar( 'sidebar-1' ) )

		wp_enqueue_script( 'jquery-masonry' );



	// Loads JavaScript file with functionality specific to Twenty Thirteen.

	wp_enqueue_script( 'twentythirteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2014-06-08', true );



	// Add Source Sans Pro and Bitter fonts, used in the main stylesheet.

	wp_enqueue_style( 'twentythirteen-fonts', twentythirteen_fonts_url(), array(), null );



	// Add Genericons font, used in the main stylesheet.

	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.03' );



	// Loads our main stylesheet.

	wp_enqueue_style( 'twentythirteen-style', get_stylesheet_uri(), array(), '2013-07-18' );



	// Loads the Internet Explorer specific stylesheet.

	wp_enqueue_style( 'twentythirteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentythirteen-style' ), '2013-07-18' );

	wp_style_add_data( 'twentythirteen-ie', 'conditional', 'lt IE 9' );

}

add_action( 'wp_enqueue_scripts', 'twentythirteen_scripts_styles' );



/**

 * Filter the page title.

 *

 * Creates a nicely formatted and more specific title element text for output

 * in head of document, based on current view.

 *

 * @since Twenty Thirteen 1.0

 *

 * @param string $title Default title text for current view.

 * @param string $sep   Optional separator.

 * @return string The filtered title.

 */

function twentythirteen_wp_title( $title, $sep ) {

	global $paged, $page;



	if ( is_feed() )

		return $title;



	// Add the site name.

	$title .= get_bloginfo( 'name', 'display' );



	// Add the site description for the home/front page.

	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) )

		$title = "$title $sep $site_description";



	// Add a page number if necessary.

	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )

		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) );



	return $title;

}

add_filter( 'wp_title', 'twentythirteen_wp_title', 10, 2 );





if ( ! function_exists( 'twentythirteen_paging_nav' ) ) :

/**

 * Display navigation to next/previous set of posts when applicable.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_paging_nav() {

	global $wp_query;



	// Don't print empty markup if there's only one page.

	if ( $wp_query->max_num_pages < 2 )

		return;

	?>

<nav class="navigation paging-navigation" role="navigation">
  <h1 class="screen-reader-text">
    <?php _e( 'Posts navigation', 'twentythirteen' ); ?>
  </h1>
  <div class="nav-links">
    <?php if ( get_next_posts_link() ) : ?>
    <div class="nav-previous">
      <?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentythirteen' ) ); ?>
    </div>
    <?php endif; ?>
    <?php if ( get_previous_posts_link() ) : ?>
    <div class="nav-next">
      <?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
    </div>
    <?php endif; ?>
  </div>
  <!-- .nav-links -->
</nav>
<!-- .navigation -->
<?php

}

endif;



if ( ! function_exists( 'twentythirteen_post_nav' ) ) :

/**

 * Display navigation to next/previous post when applicable.

*

* @since Twenty Thirteen 1.0

*/

function twentythirteen_post_nav() {

	global $post;



	// Don't print empty markup if there's nowhere to navigate.

	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );

	$next     = get_adjacent_post( false, '', false );



	if ( ! $next && ! $previous )

		return;

	?>
<nav class="navigation post-navigation" role="navigation">
  <h1 class="screen-reader-text">
    <?php _e( 'Post navigation', 'twentythirteen' ); ?>
  </h1>
  <div class="nav-links">
    <?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'twentythirteen' ) ); ?>
    <?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen' ) ); ?>
  </div>
  <!-- .nav-links -->
</nav>
<!-- .navigation -->
<?php

}

endif;



if ( ! function_exists( 'twentythirteen_entry_meta' ) ) :

/**

 * Print HTML with meta information for current post: categories, tags, permalink, author, and date.

 *

 * Create your own twentythirteen_entry_meta() to override in a child theme.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_entry_meta() {

	if ( is_sticky() && is_home() && ! is_paged() )

		echo '<span class="featured-post">' . __( 'Sticky', 'twentythirteen' ) . '</span>';



	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )

		twentythirteen_entry_date();



	// Translators: used between list items, there is a space after the comma.

	$categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );

	if ( $categories_list ) {

		echo '<span class="categories-links">' . $categories_list . '</span>';

	}



	// Translators: used between list items, there is a space after the comma.

	$tag_list = get_the_tag_list( '', __( ', ', 'twentythirteen' ) );

	if ( $tag_list ) {

		echo '<span class="tags-links">' . $tag_list . '</span>';

	}



	// Post author

	if ( 'post' == get_post_type() ) {

		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',

			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),

			esc_attr( sprintf( __( 'View all posts by %s', 'twentythirteen' ), get_the_author() ) ),

			get_the_author()

		);

	}

}

endif;



if ( ! function_exists( 'twentythirteen_entry_date' ) ) :

/**

 * Print HTML with date information for current post.

 *

 * Create your own twentythirteen_entry_date() to override in a child theme.

 *

 * @since Twenty Thirteen 1.0

 *

 * @param boolean $echo (optional) Whether to echo the date. Default true.

 * @return string The HTML-formatted post date.

 */

function twentythirteen_entry_date( $echo = true ) {

	if ( has_post_format( array( 'chat', 'status' ) ) )

		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen' );

	else

		$format_prefix = '%2$s';



	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',

		esc_url( get_permalink() ),

		esc_attr( sprintf( __( 'Permalink to %s', 'twentythirteen' ), the_title_attribute( 'echo=0' ) ) ),

		esc_attr( get_the_date( 'c' ) ),

		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )

	);



	if ( $echo )

		echo $date;



	return $date;

}

endif;



if ( ! function_exists( 'twentythirteen_the_attached_image' ) ) :

/**

 * Print the attached image with a link to the next attached image.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_the_attached_image() {

	/**

	 * Filter the image attachment size to use.

	 *

	 * @since Twenty thirteen 1.0

	 *

	 * @param array $size {

	 *     @type int The attachment height in pixels.

	 *     @type int The attachment width in pixels.

	 * }

	 */

	$attachment_size     = apply_filters( 'twentythirteen_attachment_size', array( 724, 724 ) );

	$next_attachment_url = wp_get_attachment_url();

	$post                = get_post();



	/*

	 * Grab the IDs of all the image attachments in a gallery so we can get the URL

	 * of the next adjacent image in a gallery, or the first image (if we're

	 * looking at the last image in a gallery), or, in a gallery of one, just the

	 * link to that image file.

	 */

	$attachment_ids = get_posts( array(

		'post_parent'    => $post->post_parent,

		'fields'         => 'ids',

		'numberposts'    => -1,

		'post_status'    => 'inherit',

		'post_type'      => 'attachment',

		'post_mime_type' => 'image',

		'order'          => 'ASC',

		'orderby'        => 'menu_order ID'

	) );



	// If there is more than 1 attachment in a gallery...

	if ( count( $attachment_ids ) > 1 ) {

		foreach ( $attachment_ids as $attachment_id ) {

			if ( $attachment_id == $post->ID ) {

				$next_id = current( $attachment_ids );

				break;

			}

		}



		// get the URL of the next image attachment...

		if ( $next_id )

			$next_attachment_url = get_attachment_link( $next_id );



		// or get the URL of the first image attachment.

		else

			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );

	}



	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',

		esc_url( $next_attachment_url ),

		the_title_attribute( array( 'echo' => false ) ),

		wp_get_attachment_image( $post->ID, $attachment_size )

	);

}

endif;



/**

 * Return the post URL.

 *

 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or

 * the first link found in the post content.

 *

 * Falls back to the post permalink if no URL is found in the post.

 *

 * @since Twenty Thirteen 1.0

 *

 * @return string The Link format URL.

 */

function twentythirteen_get_link_url() {

	$content = get_the_content();

	$has_url = get_url_in_content( $content );



	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );

}



/**

 * Extend the default WordPress body classes.

 *

 * Adds body classes to denote:

 * 1. Single or multiple authors.

 * 2. Active widgets in the sidebar to change the layout and spacing.

 * 3. When avatars are disabled in discussion settings.

 *

 * @since Twenty Thirteen 1.0

 *

 * @param array $classes A list of existing body class values.

 * @return array The filtered body class list.

 */

function twentythirteen_body_class( $classes ) {

	if ( ! is_multi_author() )

		$classes[] = 'single-author';



	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )

		$classes[] = 'sidebar';



	if ( ! get_option( 'show_avatars' ) )

		$classes[] = 'no-avatars';



	return $classes;

}

add_filter( 'body_class', 'twentythirteen_body_class' );



/**

 * Adjust content_width value for video post formats and attachment templates.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_content_width() {

	global $content_width;



	if ( is_attachment() )

		$content_width = 724;

	elseif ( has_post_format( 'audio' ) )

		$content_width = 484;

}

add_action( 'template_redirect', 'twentythirteen_content_width' );



/**

 * Add postMessage support for site title and description for the Customizer.

 *

 * @since Twenty Thirteen 1.0

 *

 * @param WP_Customize_Manager $wp_customize Customizer object.

 */

function twentythirteen_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';

	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';

	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

}

add_action( 'customize_register', 'twentythirteen_customize_register' );



/**

 * Enqueue Javascript postMessage handlers for the Customizer.

 *

 * Binds JavaScript handlers to make the Customizer preview

 * reload changes asynchronously.

 *

 * @since Twenty Thirteen 1.0

 */

function twentythirteen_customize_preview_js() {

	wp_enqueue_script( 'twentythirteen-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );

}

add_action( 'customize_preview_init', 'twentythirteen_customize_preview_js' );?>
<?php

// SLIDESHOW POST TYPE

function phi_post_type_News() {

	register_post_type('News', 

				array(

				'label' => __('News'),

				'singular_label' => __('News'),

				'public' => true,

				'show_ui' => true,

				'_builtin' => false, // It's a custom post type, not built in

				'_edit_link' => 'post.php?post=%d',

				'capability_type' => 'post',

				'hierarchical' => false,

				'rewrite' => array("slug" => "News"), // Permalinks

				'query_var' => "phiNews", // This goes to the WP_Query schema

				'supports' => array('title','author','thumbnail','excerpt','editor','custom-fields','comments'),

				'menu_position' => 5,

				'publicly_queryable' => true,

				'exclude_from_search' => true,

				));

}

// Add post type if not disabled in theme options

if(get_option('phi_cp_News_disable')==false){	

add_action('init', 'phi_post_type_News');

}

?>
<?php

//image resize

if ( function_exists( 'add_image_size' ) ) { 

	add_image_size('News', 100, 100, true);

}

?>
<?php

// SLIDESHOW POST TYPE

function phi_post_type_Videos() {

	register_post_type('Videos', 

				array(

				'label' => __('Videos'),

				'singular_label' => __('Videos'),

				'public' => true,

				'show_ui' => true,

				'_builtin' => false, // It's a custom post type, not built in

				'_edit_link' => 'post.php?post=%d',

				'capability_type' => 'post',

				'hierarchical' => false,

				'rewrite' => array("slug" => "Videos"), // Permalinks

				'query_var' => "phiVideos", // This goes to the WP_Query schema

				'supports' => array('title','thumbnail'),

				'menu_position' => 5,

				'publicly_queryable' => true,

				'exclude_from_search' => true,

				));

}

// Add post type if not disabled in theme options

if(get_option('phi_cp_Videos_disable')==false){	

add_action('init', 'phi_post_type_Videos');

}

?>
<?php

//image resize

if ( function_exists( 'add_image_size' ) ) { 

	add_image_size('Videos', 100, 100, true);

}

?>
<?php

// SLIDESHOW POST TYPE

function phi_post_type_Radio() {

	register_post_type('Radio', 

				array(

				'label' => __('Radio'),

				'singular_label' => __('Radio'),

				'public' => true,

				'show_ui' => true,

				'_builtin' => false, // It's a custom post type, not built in

				'_edit_link' => 'post.php?post=%d',

				'capability_type' => 'post',

				'hierarchical' => false,

				'rewrite' => array("slug" => "Radio"), // Permalinks

				'query_var' => "phiRadio", // This goes to the WP_Query schema

				'supports' => array('title','author','thumbnail','excerpt','editor','custom-fields','comments'),

				'menu_position' => 5,

				'publicly_queryable' => true,

				'exclude_from_search' => true,

				));

}

// Add post type if not disabled in theme options

if(get_option('phi_cp_Radio_disable')==false){	

add_action('init', 'phi_post_type_Radio');

}

?>
<?php

//image resize

if ( function_exists( 'add_image_size' ) ) { 

	add_image_size('Radio', 100, 100, true);

}

?>
<?php

// SLIDESHOW POST TYPE

function phi_post_type_Gallery() {

	register_post_type('Gallery', 

				array(

				'label' => __('Gallery'),

				'singular_label' => __('Gallery'),

				'public' => true,

				'show_ui' => true,

				'_builtin' => false, // It's a custom post type, not built in

				'_edit_link' => 'post.php?post=%d',

				'capability_type' => 'post',

				'hierarchical' => false,

				'rewrite' => array("slug" => "Gallery"), // Permalinks

				'query_var' => "phiGallery", // This goes to the WP_Query schema

				'supports' => array('title','thumbnail'),

				'menu_position' => 5,

				'publicly_queryable' => true,

				'exclude_from_search' => true,

				));

}

// Add post type if not disabled in theme options

if(get_option('phi_cp_Gallery_disable')==false){	

add_action('init', 'phi_post_type_Gallery');

}

?>
<?php

//image resize

if ( function_exists( 'add_image_size' ) ) { 

	add_image_size('Gallery', 100, 100, true);

}

?>
<?php   if(function_exists('register_sidebar'))

    

	register_sidebar(array(

	'name'=>'Right Sidebar',

        'before_widget' => '<div>',

        'after_widget' => '</div>',

        'before_title' => '<span class="innheadingtxt" style="font-size:26px;">',

        'after_title' => '</span>',

    ));

	

    register_sidebar(array(

	'name'=>'Testimonials',

        'before_widget' => '<div>',

        'after_widget' => '</div>',

        'before_title' => '<h2 class="widgettitle">',

        'after_title' => '</h2>',

    ));
	   register_sidebar(array(

	'name'=>'Socal',

        'before_widget' => '<div>',

        'after_widget' => '</div>',

        'before_title' => '<h2 class="widgettitle">',

        'after_title' => '</h2>',

    ));

	

?>
<?php

///////////////////////////////////LOGING IMAGE CODE////////////////////////////////

//function my_custom_login_logo() {

   // echo '<style type="text/css">

       // h1 a { background-image:url('.get_bloginfo('template_directory').'/images/logo.png) !important; }

   // </style>';

//}

//add_action('login_head', 'my_custom_login_logo');

?>
<?php 

    ////////////////////////////////////////////////////////// GET FEATURED IMAGE  ///////////////////////

    function ST4_get_featured_image($post_ID) {  

        $post_thumbnail_id = get_post_thumbnail_id($post_ID);  

        if ($post_thumbnail_id) {  

            $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');  

            return $post_thumbnail_img[0];  

        }  

    }  

	    // ADD NEW COLUMN  

    function ST4_columns_head($defaults) {  

        $defaults['featured_image'] = 'Featured Image';  

        return $defaults;  

    }  

    // SHOW THE FEATURED IMAGE  

    function ST4_columns_content($column_name, $post_ID) {  

        if ($column_name == 'featured_image') {  

            $post_featured_image = ST4_get_featured_image($post_ID);  

            if ($post_featured_image) {  

                echo '<img src="' . $post_featured_image . '" width="150" height="100"/>';  

            }  

        }  

    }  

	    add_filter('manage_posts_columns', 'ST4_columns_head');  

    add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2); 

	add_action('init', 'ST4_add_movies');  

function ST4_add_movies() {  

    $args = array(  

        'label' => __('Movies'),  

        'singular_label' => __('Movie'),  

        'public' => true,  

        'show_ui' => true,  

        'capability_type' => 'post',  

        'hierarchical' => false,  

        'rewrite' => true,  

        'supports' => array('title', 'editor', 'thumbnail')  

    );  

    register_post_type( 'movie' , $args );  

}

add_filter('manage_posts_columns', 'ST4_columns_head');  

add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);

// ONLY WORDPRESS DEFAULT PAGES  

add_filter('manage_page_posts_columns', 'ST4_columns_head', 10);  

add_action('manage_page_posts_custom_column', 'ST4_columns_content', 10, 2); 

?>
<?php 

function ds_get_excerpt($num_chars) {

    $temp_str = substr(strip_tags(get_the_content()),0,$num_chars);

    $temp_parts = explode(" ",$temp_str);

    $temp_parts[(count($temp_parts) - 1)] = '';

    

    if(strlen(strip_tags(get_the_content())) > 125)

      return implode(" ",$temp_parts) . '...';

    else

      return implode(" ",$temp_parts);

}

function ds_get_excerpt2($num_chars, $post_link) {

    $temp_str = substr(strip_tags(get_the_content()),0,$num_chars);

    $temp_parts = explode(" ",$temp_str);

    $temp_parts[(count($temp_parts) - 1)] = '';

    

    if(strlen(strip_tags(get_the_content())) > 125)

      return implode(" ",$temp_parts) . '<a href="' . $post_link . '">[read more ...]</a>';

    else

      return implode(" ",$temp_parts);

}

if (function_exists('add_theme_support')) {

	add_theme_support('menus');

}

function get_category_id($cat_name){

	$term = get_term_by('name', $cat_name, 'category');

	return $term->term_id;

}

function catch_that_image() {

  global $post, $posts;

  $first_img = '';

  ob_start();

  ob_end_clean();

  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image

    $first_img = "/images/post_default.png";

  }

  return $first_img;

}

?>
<?php function commentCount() {

    global $wpdb;

    $count = $wpdb->get_var('SELECT COUNT(comment_ID) FROM ' . $wpdb->comments. ' WHERE comment_author_email = "' . get_comment_author_email() . '"');

    echo $count . ' comments';

}?>
<?php 

//ENABLE SHORTCODES IN THE TEXT WIDGET

add_filter( 'widget_text', 'do_shortcode' );

?>
<?php add_theme_support( 'automatic-feed-links')?>
<?php add_filter( 'show_admin_bar', '__return_false' ); ?>
<?php add_theme_support('custom-header')?>
<?php  add_theme_support('custom-background', $args )?>
<?php add_editor_style()?>
<?php 

// WORDPRESS UP DATE VERSION NI DISABLE CHAYEE DANIKI  EE CODE VADUTHAMU

add_action('admin_menu','wphidenag');

function wphidenag() {

remove_action( 'admin_notices', 'update_nag', 3 );

}

?>
<?php 

// PLUGINS UP DATE VERSION NI DISABLE CHAYEE DANIKI  EE CODE VADUTHAMU

remove_action( 'load-update-core.php', 'wp_update_plugins' );

add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

?>
<?php 

////////IMAGE URL///////<img alt="" src="[Theme_Url]/images/dates_mini_bnnr.png" />//////////////

add_shortcode( 'Theme_Url', 'get_Theme_Url' );

function get_Theme_Url($atts, $content=null) {

return get_bloginfo('template_url')	;

}

?>
<?php

////////WIDGET PHP CODE///////widgets lo php code support chayeedaniki />//////////////

add_filter('widget_text','execute_php',100);

}  
  
add_filter('admin_footer_text', 'change_footer_admin');

function execute_php($html){

if(strpos($html,"<"."?php")!==false){

ob_start();

eval("?".">".$html);

$html=ob_get_contents();

ob_end_clean();

}

return $html;

}

?>
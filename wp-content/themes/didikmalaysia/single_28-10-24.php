<?php get_header(); ?>
<br/>
<section id="aboutus">
  <div class="container-full">
    <div class="container">
      <div class="heading">
        <h3>
          <?php the_title();?>
        </h3>
      </div>
      <!-- page content
    ================================================== -->
      <div id="body-right">
        <style>
.link-pages {
     clear:both;
}
</style>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h2></h2>
          <div class="entry-content">
            <?php if(have_posts()) : while(have_posts()) : the_post();?>
            <?php the_content(); ?>
            <?php wp_link_pages( 'before=<p class="link-pages">Page: ' );?>
            <div class="entry-meta">
              <?php twentythirteen_entry_meta(); ?>
              <?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
            </div>
            <!-- .entry-meta -->
            <?php endwhile; endif; ?>
          </div>
          <!-- .entry-content -->
        </div>
      </div>
      <!-- end page content
    ================================================== -->
      <!-- /#aboutusleft -->
      <!-- /#aboutusright -->
    </div>
    <!-- /.container -->
    <div class="clear-bottom"></div>
  </div>
  <!-- /.container-full -->
</section>
<?php get_footer(); ?>
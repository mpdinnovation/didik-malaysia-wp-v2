<?php /*Template Name:contact-Page*/?>
<?php get_header();?>
<div class="innercontainer contact-page">
 <div class="padd">
 
 <div class="col-12">
     <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>
<center><h2><?php the_title();?></h2></center><div class="container" style="border-top:0.5px dashed #CCCCCC !important;padding-top:22px;" ></div>
     <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <?php if(have_posts()) : while(have_posts()) : the_post();?>
         
          <?php the_content(); ?>
		  
		  
		  
          <!-- .entry-meta -->
          <?php endwhile; endif; ?>
        </div>
        <!-- .entry-content -->
      </div>

</div>

</div>
</div>
<?php get_footer();?>
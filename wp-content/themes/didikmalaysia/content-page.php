<?php get_header();?>
<div class="innercontainer">
 <div class="padd" style="padding:20px !important;">
 <div class="col-10"  >
  <h2><?php the_title();?></h2>
      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <?php if(have_posts()) : while(have_posts()) : the_post();?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <img src="<?php echo $src[0];?>" alt="" style="width:350px;height:280px;padding:8px;" align="right" >
         <?php /*?> <p style="color:#D2474D;">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </p><?php */?>
          <?php the_content(); ?>
		
          <!-- .entry-meta -->
          <?php endwhile; endif; ?>
 
 </div>
</div>

<!--<div style="background:#000; height:500px;">
<div class="innercol1">&nbsp;</div>
<div style="background:#ccc; height:30px; float:left; width:30%; margin:5px;">&nbsp;</div>
<div style="background:#ccc; height:30px; float:left; width:30%; margin:5px;">&nbsp;</div>
<div style="clear::both;"></div>
</div>-->

</div>
</div>
</div>
<?php get_footer();?>

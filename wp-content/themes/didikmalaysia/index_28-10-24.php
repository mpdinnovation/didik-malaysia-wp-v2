<?php /*Template Name:Home-page*/?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta charset="UTF-8">
<meta name="description" content="Didik Malaysia" >
<meta name="author" content="CodexCoder">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="all" />
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<section id="slider">
  <div style="text-align:center; z-index:999; position:absolute; top:280px; width:100%">
    <div align="center"><a href="<?php echo home_url(); ?>/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a></div>
  </div>
  <div style="position:relative; width:1000px; margin:0 auto;">
    <div style="position:relative;">
      <div style="z-index:999; position:absolute; top:30px; left:0px; color:#fff;">Email:demo@mail.com</div>
    </div>
    <div style="position:relative;">
      <div style="z-index:999; position:absolute; top:30px; right:0px; color:#fff;"><a href="#aboutus" class="topalink">About us</a> | <a href="#somework" class="topalink">News</a> | <a href="#contactus" class="topalink">Contact</a></div>
    </div>
  </div>
  <div class="container-full"> <?php echo do_shortcode('[rev_slider Home-page-slider]'); ?>
  </div>
  <!-- /.container-full -->
</section>
<!-- /#Slider -->
<header id="header">
  <section id="headnev2" class="navbar topnavbar" >
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
      </div>
      <!-- /.navbar-header -->
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a class="somework" href="#somework"><img src="<?php echo get_template_directory_uri(); ?>/img/news.png" alt="news"></a></li>
          <li><a class="portfolio" href="#portfolio"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery.png" alt="news"></a></li>
          <li><a class="team" href="#team"><img src="<?php echo get_template_directory_uri(); ?>/img/video.png" alt="news"></a></li>
          <li><a class="pricing" href="#pricing"><img src="<?php echo get_template_directory_uri(); ?>/img/radio.png" alt="news"></a></li>
          <li><a  href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/notification.png" alt="news"></a></li>
        </ul>
        <div class="">
          <div style="position:relative;">
            <div style="position:absolute; top:0px; left:208px; z-index:9999;"><img src="<?php echo get_template_directory_uri(); ?>/img/divider.png"></div>
          </div>
          <div style="position:absolute;">
            <div style="position:relative; top:0px; left:426px; z-index:9999;"><img src="<?php echo get_template_directory_uri(); ?>/img/divider.png"></div>
          </div>
          <div style="position:absolute;">
            <div style="position:relative; top:0px; left:609px; z-index:9999;"><img src="<?php echo get_template_directory_uri(); ?>/img/divider.png"></div>
          </div>
          <div style="position:absolute;">
            <div style="position:relative; top:0px; left:799px; z-index:9999;"><img src="<?php echo get_template_directory_uri(); ?>/img/divider.png"></div>
          </div>
        </div>
      </div>
      <!-- /.collapse /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </section>
  <!-- /#headnev -->
</header>
<!-- /#Header-->
<br>
<section id="somework">
  <div class="worksection">
    <div class="container-full">
      <div class="works">
        <div class="container">
          <h3 class="newsubheadtext">News</h3>
        </div>
      </div>
      <!-- /.works -->
      <div class="container">
        <div class="works-img">
          <div id="work-slide" class="carousel slide">
            <div class="slide2nev"> <a class="carousel-control left" href="#work-slide" data-slide="prev"><i class="fa fa-chevron-left"></i></a> <a class="carousel-control right" href="#work-slide" data-slide="next"><i class="fa fa-chevron-right"></i></a> </div>
            <!-- /.slide2nev -->
            <!-- Slider items -->
            <div class="carousel-inner">
              <div class="active item">
                <?php
$queryObject = new WP_Query( 'post_type=News&posts_per_page=4' );
if ($queryObject->have_posts()) {
    ?>
                <?php
    while ($queryObject->have_posts()) {

        $queryObject->the_post();
        ?>
                <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <div class="col-sm-3">
                  <figure> <img src="<?php echo $src[0];?>" alt="" style="width:255px;height:163px;">
                    <?php /*?><img src="<?php echo get_template_directory_uri(); ?>/img/9.jpg" alt="9"><?php */?>
                    <figcaption>
                      <h4>
                        <?php the_title();?>
                      </h4>
                    </figcaption>
                  </figure>
                  <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
                    <?php the_title();?>
                    </a></h4>
                  <p style="text-align:justify;">
                    <?php $cont = get_the_content(); echo substr($cont,0,155); ?>
                  </p>
                  <div><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" style="text-decoration:none;color:#006633;"><strong>View +</strong></a></div>
                </div>
                <?php


    }
    ?>
                <?php
}
?>
              </div>
              <div class="item">
                <?php
$queryObject = new WP_Query( 'post_type=News&posts_per_page=4' );
if ($queryObject->have_posts()) {
    ?>
     <?php
    while ($queryObject->have_posts()) {
        $queryObject->the_post();
        ?>
                <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <div class="col-sm-3">
                  <figure> <img src="<?php echo $src[0];?>" alt="" style="width:255px;height:163px;">
                    <?php /*?><img src="<?php echo get_template_directory_uri(); ?>/img/9.jpg" alt="9"><?php */?>
                    <figcaption>
                      <h4>
                        <?php the_title();?>
                      </h4>
                    </figcaption>
                  </figure>
                  <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
                    <?php the_title();?>
                    </a></h4>
                  <p style="text-align:justify;">
                    <?php $cont = get_the_content(); echo substr($cont,0,155); ?>
                  </p>
                  <div><a href="#" rel="bookmark" title="<?php the_title(); ?>" style="text-decoration:none;color:#006633;"><strong>View +</strong></a></div>
                </div>
                <?php

    }
    ?>
                <?php
}
?>
              </div>
              <!-- /.active /.item -->
              <!-- /.item -->
            </div>
            <!-- /.carousel-inner -->
            <!--/.carousel -->
          </div>
          <!-- /#work-slide -->
        </div>
        <!-- /.works-img -->
      </div>
      <!--/.container-->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.worksection -->
</section>
<!-- /#somework -->
<header id="header">
  <section id="headnev" class="navbar topnavbar" >
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button>
        <a href="#header" class="navbar-brand header"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png"></a> </div>
      <!-- /.navbar-header -->
      <div class="collapse navbar-collapse">
        <ul class="nav pull-right navbar-nav">
          <li><a class="slider" href="#slider">Home</a></li>
          <li><a class="somework" href="#somework">News</a></li>
          <li><a class="portfolio" href="#portfolio">Gallery</a></li>
          <li><a class="team" href="#team">Video</a></li>
          <li><a class="pricing" href="#pricing">Radio</a></li>
          <li><a class="contactus" href="#contactus">Contact</a></li>
        </ul>
      </div>
      <!-- /.collapse /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </section>
  <!-- /#headnev -->
</header>
<!-- /#Header-->
<!-- /#aboveportfolio -->
<section id="portfolio">
  <div class="container-full">
    <div class="container">
      <h2 class="newsubheadtext">Gallery</h2>
      <div class="portfolio-type portfolio-nav">
        <ul class="list-inline">
          <li><a class="filter" data-filter="all">All</a></li>
          <li><a class="filter" data-filter="category_1">Website</a></li>
          <li><a class="filter" data-filter="category_2">Application</a></li>
          <li><a class="filter" data-filter="category_3">Branding</a></li>
          <li><a class="filter" data-filter="category_3 category_1">Photography</a></li>
        </ul>
      </div>
      <!-- /.portfolio-type -->
    </div>
    <!-- /.container -->
    <div class="container">
      <div class="portfolio-img">
        <ul id="Grid">
          <li class="mix category_3" data-cat="3" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/8.jpg" alt="8">
              <figcaption>
                <h4>Love</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_2" data-cat="2" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/9.jpg" alt="9">
              <figcaption>
                <h4>Christmas Gift</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_1" data-cat="1" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/2.jpg" alt="2">
              <figcaption>
                <h4>Care</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_2" data-cat="2" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/3.jpg" alt="3">
              <figcaption>
                <h4>Pizza</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_1" data-cat="1" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/4.jpg" alt="4">
              <figcaption>
                <h4>Herbal</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_3" data-cat="3" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/5.jpg" alt="5">
              <figcaption>
                <h4>Thinking</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_1" data-cat="1" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/6.jpg" alt="6">
              <figcaption>
                <h4>Career</h4>
              </figcaption>
            </figure>
          </li>
          <li class="mix category_1" data-cat="1" >
            <figure> <img src="<?php echo get_template_directory_uri(); ?>/img/7.jpg" alt="7">
              <figcaption>
                <h4>Gift</h4>
              </figcaption>
            </figure>
          </li>
        </ul>
        <!-- /#Grid -->
      </div>
      <!-- /.portfolio-img -->
    </div>
    <div class="clear-bottom"></div>
    <!-- /.container -->
  </div>
  <!-- /.container-full -->
</section>
<!-- #portfolio -->
<section id="team">
  <div class="worksection">
    <div class="container-full">
      <!-- /.works -->
      <div class="container">
        <h3 class="newsubheadtext">Videos</h3>
        <ul>
          <?php



$queryObject = new WP_Query( 'post_type=Videos&posts_per_page=4' );



// The Loop!



//custom postname : videos



if ($queryObject->have_posts()) {



    ?>
          <?php



    while ($queryObject->have_posts()) {



        $queryObject->the_post();



        ?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <li class="col-xs-3">
            <div> <a href=" <?php echo do_shortcode('[types field="add-video-link" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>"> <img src="<?php echo $src[0];?>" alt="" style="width:265px;height:173px;"></a>
              <p class="faicons"> </p>
              <h4>
                <?php the_title();?>
              </h4>
              <p style="text-align:justify;">
                <?php $cont = get_the_content(); echo substr($cont,0,55); ?>
              </p>
            </div>
          </li>
          <?php



    }



    ?>
          <?php



}



?>
        </ul>
      </div>
      <!--/.container-->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.worksection -->
</section>
<!-- /#Service -->
<section id="pricing">
  <div class="container-full">
    <div class="container">
      <h3 class="newsubheadtext">Radio</h3>
      <ul>
        <?php



$queryObject = new WP_Query( 'post_type=Radio&posts_per_page=4' );



// The Loop!



//custom postname : videos



if ($queryObject->have_posts()) {



    ?>
        <?php



    while ($queryObject->have_posts()) {



        $queryObject->the_post();



        ?>
        <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
        <li class="col-xs-3">
          <div> <a href=" <?php echo do_shortcode('[types field="add-radio-links" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>"> <img src="<?php echo $src[0];?>" alt="" style="width:265px;height:173px;"></a>
            <p class="faicons"> </p>
            <h4>
              <?php the_title();?>
            </h4>
            <p style="text-align:justify;">
              <?php $cont = get_the_content(); echo substr($cont,0,55); ?>
            </p>
          </div>
        </li>
        <?php



    }



    ?>
        <?php



}



?>
      </ul>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.container-full -->
</section>
<section id="aboutus">
  <div class="container-full">
    <div class="container">
      <div id="aboutusleft" class="col-xs-12">
        <div class="heading">
          <h3 class="newsubheadtext">About Us</h3>
        </div>
        <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
          
          
          
          Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>
      </div>
      <!-- /#aboutusleft -->
      <!-- /#aboutusright -->
    </div>
    <!-- /.container -->
    <div class="clear-bottom"></div>
  </div>
  <!-- /.container-full -->
</section>
<section id="testimonial">
  <div class="testsection">
    <div class="container-full">
      <div class="heading">
        <div class="container">
          <h3 class="newsubheadtext">Testimonial</h3>
        </div>
      </div>
      <!-- /.heading -->
      <div class="container">
        <?php dynamic_sidebar('Testimonials')?>
      </div>
      <!-- /.container -->
    </div>
    <!-- /.container-full -->
  </div>
  <!-- /.testsection -->
</section>
<!-- /#testimonial -->
<section id="contactus">
  <div class="container-full">
    <!-- <div id="map_canvas"></div> /#map_canvas -->
    <iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 



src="https://maps.google.com.bd/maps?q=dhaka+on+google+map&amp;ie=UTF8&amp;hq=&amp;gl=bd&amp;ll=23.709921,90.407143&amp;spn=0.734303,1.352692&amp;t=m&amp;z=10&amp;iwloc=A&amp;output=embed"> </iframe>
    <div class="shadowcontainer">
      <div class="shadow">
        <div class="clear-top"></div>
        <div class="container">
          <div class="col-md-6 pull-left"> <?php echo do_shortcode(' [contact-form-7 id="15" title="Contact form 1"]'); ?> </div>
          <!-- /.col-md-6 -->
          <div class="col-md-3 gettouch pull-right">
            <h4>Get in touch</h4>
            <p class="email"> info@didikmalaysia.com </p>
            <p> info@didikmalaysia.com </p>
            <p class="email"> Web </p>
            <p> www.didikmalaysia.com </p>
            <p class="address"> Address </p>
            <p> Test, Street
              
              
              
              Malaysia </p>
          </div>
          <!-- /.col-md-3 /.gettouch -->
        </div>
        <!-- /.container -->
      </div>
      <!-- /.shadow -->
    </div>
    <!-- /.shadowcontainer -->
  </div>
  <!-- /.container-full -->
</section>
<!-- /.contactus-->
<!--   /#scroller  -->
<section id="scroller2">
  <div class="gotop"> <a class="header" href="#header"><i class="fa fa-arrow-circle-o-up"></i></a> </div>
  <!-- /.gotop -->
</section>
<!--   /#scroller2  -->
<section id="footer">
  <footer class="container-full">
    <div class="container">
      <!-- /.col-md-3 -->
    </div>
    <!-- /.container -->
    <div class="clear-bottom copyright">
      <p>&copy; Copyright 2014, All Rights Reserved. Powered by <a href="http://didikmalaysia.com">didikmalaysia</a></p>
    </div>
    <!-- /.clear-bottom /.copyright -->
  </footer>
  <!-- /.container-full -->
</section>
<!-- /.footer -->
</body>
</html>

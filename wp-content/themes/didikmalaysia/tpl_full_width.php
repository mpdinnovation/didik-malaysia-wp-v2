<?php
/**
* Template name: Full Width Page
*/
?>
<?php get_header(); ?>
    <div class="innercontainer">
    	<div class="padd">
        <div class="glanguage">
        <div id="google_translate_element"></div>
        <script type="text/javascript">
        function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
        }
        </script> 
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style> 
        </div>
        <h2><?php the_title();?></h2>
        <div class="page-content">
            <style>
            .link-pages{clear:both;}
            </style>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="entry-content">
                    <?php if(have_posts()) : while(have_posts()) : the_post();?>
                    <?php the_content(); ?>
                    <?php wp_link_pages( 'before=<p class="link-pages">Page: ' );?>
                        <div class="entry-meta">
                        <?php twentythirteen_entry_meta(); ?>
                        <?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
                        </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div> 
        </div><!--padd-->
    </div><!--container-->
<?php get_footer();?>

<?php get_header();?>
<style>
  .single-page-content{width:100%; float:left; border-top:2px dashed #CCCCCC; padding:22px 0 0 0;}
  .single-featured{width:100%; float:left;}
  .single-featured img{float:none; padding:10px 0;}
  .single-page-content p{padding:0 10px;}  
  </style>


<div class="innercontainer">
 <div class="padd">
     <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>
<h2><?php the_title();?></h2>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">
          <?php if(have_posts()) : while(have_posts()) : the_post();?>

       <div style="color:#696969"> 
     <p style="color:#696969; display:inline;">By<p style="color:#ee7600; display:inline;">
            <?php the_author(); ?>
             <p style="color:#696969; display:inline;">|
             <p style="color:#0f6eac; display:inline;">
            <?php the_time('M d, Y') ?>  <p style="color:#696969; display:inline;">| <?php the_tags( '<p class="tag-anchor tag-anchor1" style="color:#D2474D; display:inline;">', ', ', '</p>'); ?> |<p style="color:#94dc4a; display:inline;"> <?php if(function_exists('the_views')) { the_views(); } ?></p>
            </div>
          
   <div class="single-featured">

          <?php the_post_thumbnail('single-featured'); ?>
     
     </div>
          
        
          </p><br></br><div class="single-page-content">

          <?php the_content(); ?>
          <div style=" margin-top: 10px; padding-top: 6px;  width: 100%;"><br></br>
		<div class="container" style="border-top:1px dashed #CCCCCC !important;padding-top:22px;" ></div>
			<div align="left">
  
     <font color="#696969"><h6>SHARE ON</h6></font>
</div><div class="btn-group"><div align="center">
	 <a class="fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a>
	 
	  <a class="tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a><br></br>
	  </div><div class="container" style="border-top:1px dashed #CCCCCC !important;padding-top:22px;" ></div
  
  
 ><section id="news" class="news-section" >
    <div class="container">
      <h2>Related Posts</h2>
        <div class="news-wrap">
			<div class="news-slider">
                <?php
                $args = array(
                'post_type' => array('news_post','Videos','radio'),
                'showposts'=> 12,
                'paged' => $paged,
                );
                
                $queryObject= null;
                $queryObject = new WP_Query();
                $queryObject->query($args);
                
                if ($queryObject->have_posts()) {
                while ($queryObject->have_posts()) {
                $queryObject->the_post();
                $val_post_type=get_post_type( $post->ID);
                ?>
                <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <div class="news-onehome">
                
                <?php the_post_thumbnail('home-video'); ?>
                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                <h5>Posted by <?php the_author(); ?> | <?php the_time('M d, Y') ?></h5>
                <div class="cont">
  <div style="padding-top:6px;">
    <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>
    </div>
  </div>
</div>  
                </div>
                <?php
                }
                }
                ?>
              </div>
        </div>
    <div>
</section> 
  
          
          
			</div>
          <!-- .entry-meta -->
          <?php comments_template(); ?>
          <?php endwhile; endif; ?>
        </div>
        <!-- .entry-content -->
      </div>
</div>

</div>
</div>

<?php get_footer(); ?>
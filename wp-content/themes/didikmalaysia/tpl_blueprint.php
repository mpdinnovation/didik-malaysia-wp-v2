<?php
/**
 * Template Name: Malaysian Education Blueprint
 */

get_header(); ?>
<div class="blueprint-featured">
	<?php the_post_thumbnail('full'); ?>
    <div class="featured-content">
    	<div class="innercontainer1">
        <h3><?php echo do_shortcode('[types field="banner_text"]'); ?></h3>
    </div>
    </div><!--featured content-->
</div>

<div class="innercontainer">
	<div class="padd">	  
    <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>      
    	<h2><?php the_title();?></h2>
        
        <div class="page-content">
			<?php while (have_posts()) : the_post(); ?>
            <?php the_content(); ?>
            <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
            <?php endwhile; ?>     
    	</div><!--page content-->
	</div><!--padd-->
</div><!--container-->
<?php get_footer(); ?>

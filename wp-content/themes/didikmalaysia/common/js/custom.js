$(document).ready(function(){
	
	//Header Menu
	$('header nav').meanmenu();
	
	$(".footer-wrap ul").addClass("footer-menu");
	
	$(".gallerySlider").owlCarousel({
		navigation : true, 
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	});	
	
	$(".news-slider").owlCarousel({
		navigation : true,
		items : 3, 
		itemsDesktop : [1000,2], 
		itemsDesktopSmall : [900,2], 
		itemsMobile: [767,1], 
		autoPlay:true	
	});
	
	$(".tag-anchor a").attr("href", "#")
	
	$('#tab-container').easytabs();
	
	$('.fancybox').fancybox();
						  
	$(".radio-slider").owlCarousel({
	navigation : true,
	items : 4,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [979,3],
	autoPlay:true
	});	
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
	
	$(function() {
	if (window.PIE) {
		$('.gallery-tab, .more-video').each(function() {
			PIE.attach(this);
		});
	}
	});
		
	$('.banner-slider').bxSlider({
		
	infiniteLoop: true,
	pager: false,
	hideControlOnEnd: true,
	 onSliderLoad: function(){
$(".banner-slider").css("visibility", "visible");
} 
	});	
	
	//$('.my-dropdown').sSelect();
	//$('.operMenu').hide();
	
	$('.moreLink').click(function(){
		$(this).parents('li').find('.operMenu').slideToggle();
		$(this).parents('li').find('.moreLink').addClass('active');
		$(this).parents('li').siblings().find('.moreLink').removeClass('active');
		$(this).parents('li').siblings().find('.operMenu').slideUp();
	});

		
	
	
	
});


var widthTH= $(window).width();
if(widthTH < 379) {
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              
			  
				$('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
}

else {
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              
			  
				$('html,body').animate({
                    scrollTop: target.offset().top-116
                }, 1000);
                return false;
            }
        }
    });
});

 }
 
 $(window).scroll(function(){
  var sticky = $('body.home'),
	  scroll = $(window).scrollTop();

  if (scroll >= 80) sticky.addClass('remove-menu');
  else sticky.removeClass('remove-menu');
});
 
 $(window).scroll(function(){
  var sticky = $('.header-bottom'),
	  scroll = $(window).scrollTop();

  if (scroll >= 430) sticky.addClass('fixed-header');
  else sticky.removeClass('fixed-header');
});


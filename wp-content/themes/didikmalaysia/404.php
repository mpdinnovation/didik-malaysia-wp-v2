<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="innercontainer">
	<div id="primary" class="content-area" style="width:100%; float:left; margin:4% 0 0 0">
		<div id="content" class="site-content" role="main">

			<header class="page-header"><br></br><br></br><center>
				<h2 class="page-title" style="clear:both;"><?php _e( '<FONT COLOR="#0f6eac">Sorry</FONT>,<FONT COLOR="#D2474D"> Jim</FONT><FONT COLOR="#94dc4a"> :(</FONT>', 'twentythirteen' ); ?></h2></center>
			</header>

			<div class="page-wrapper">
				<div class="page-content"><center>
					<h2><?php _e( '<FONT COLOR="#696969">That page is absent</FONT>', 'twentythirteen' ); ?></h2>
					<p><?php _e( '<FONT COLOR="#ff8000">< Go back?</FONT>', 'twentythirteen' ); ?></p></center><br></br>

					
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->
</div>
<?php get_footer(); ?>
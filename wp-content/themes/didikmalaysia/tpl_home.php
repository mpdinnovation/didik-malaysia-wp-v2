<?php
/**
 * Template name: Homepage
 */
?>
<?php get_header(); ?>
<div class="mainContr">
  <section id="notifications" class="news-section">
    <div class="glanguage">
    <div class="container">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>
    </div>
    <!-- news-section start -->
    <div class="container"  >
      <h2 style="color:#00b200;">Notifications</h2>
      <div class="news-wrap">
        <div class="news-slider notification-slider">
          <?php
				$args = array(
				'post_type' => array('news_post','Videos','radio','Tv'),
				'showposts'=> 50,
				'paged' => $paged,
				);
				
				$queryObject= null;
				$queryObject = new WP_Query();
				$queryObject->query($args);
				
				if ($queryObject->have_posts()) {
				while ($queryObject->have_posts()) {
				$queryObject->the_post();
				$val_post_type=get_post_type( $post->ID);
				?>
				
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <div class="news-onehome">
				
				<?php
				if($val_post_type=='videos')
				{
				?>
				<a href="http://www.youtube.com/embed/<?php echo do_shortcode('[types field="add-video-link" class="" style=""][/types]'); ?>" title="<?php the_title(); ?>" target="_blank" >
				<?php
				}
				else if($val_post_type=='radio')
				{
				?>
				<a href="<?php echo do_shortcode('[types field="add-radio-links" class="" style=""][/types]'); ?>"title="<?php the_title();?>" target="_blank">
				<?php
				}
				else
				{
				?>
				<a href="<?php the_permalink() ?>" style="text-decoration:none;" > 
				<?php 
				}
				?>
		  
          <?php the_post_thumbnail('home-video'); ?>
            <h4>
              <?php the_title();
			?>
            </h4>
            <h5>Posted by
              <?php the_author(); ?>
              |
              <?php the_time('M d, Y') ?>
            </h5>
            <?php the_tags( '<p class="tag-anchor">', ', ', '</p>'); ?>

            </a>
            <div class="cont">
              <div style="padding-top:6px;">

                <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>

                </div>
              </div>
            </div>
          </div>
          <?php
    }
}
?>
        </div>
      </div>
      <br/>
    </div>
  </section>
  <section id="news" class="news-section" >
  <!-- news-section start -->
  <div class="container" style="border-top:1px dashed #CCCCCC !important;padding-top:22px;" >
    <h2>news</h2>
    <div class="news-wrap">
      <div class="news-slider">

				 <?php
				$args = array(
				'post_type' => array('news_post'),
				'showposts'=> 12,
				'paged' => $paged,
				);
				
				$queryObject= null;
				$queryObject = new WP_Query();
				$queryObject->query($args);
				
				if ($queryObject->have_posts()) {
				while ($queryObject->have_posts()) {
				$queryObject->the_post();
				$val_post_type=get_post_type( $post->ID);
				?>
        <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
        <div class="news-onehome">
	     	<?php
				 if($val_post_type=='radio')
				{
				?>
				<a href="<?php echo do_shortcode('[types field="add-radio-links" class="" style=""][/types]'); ?>"title="<?php the_title();?>" target="_blank">
				<?php
				}
				else
				{
				?>
				<a href="<?php the_permalink() ?>" style="text-decoration:none;" > 
				<?php 
				}
				?>
		
          <?php the_post_thumbnail('home-video'); ?>
          <h4>
            <?php the_title();
			?>
          </h4>
          <h5 class="blue">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </h5>

          </a>
          <div class="cont">
            <div style="padding-top:6px;">

              <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>

              </div>
            </div>
          </div>
        </div>
        <?php
    }
}
?>
      </div>
    </div>
    <br/>
    <div>
      <p>&nbsp;</p>
      <a class="more-video" href="<?php echo site_url()?>/news/">More News</a> </div>
  </div>
</div>
</section>
</div>



<section id="gallery" class="gallery-section center-gallery">
  <!-- gallery-section start -->
  <div class="container">
    <h3>Infographic</h3>
    <div class="gallery-wrap infographic-slider" id="tabs">
      <div class="header-middle" style="margin-top:0px !important;">
        <ul class="banner-slider" >
			<?php query_posts('post_type=Galleryslider&posts_per_page=10'); ?>
					
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <li><a class='fancybox' href='<?php echo $src[0];?>' data-fancybox-group='gallery'><img src="<?php echo $src[0];?>" alt=""></a></li>
<?php  endwhile; endif; wp_reset_query(); ?>

        </ul>
      </div>

    </div>
  </div>
</section>
<section id="video" class="news-section">
  <!-- video-section start -->
  <div class="container">
    <h2>Videos</h2>
    <div class="news-wrap">
      <div class="news-slider">
        <?php
      	$queryObject = new WP_Query( 'post_type=Videos&posts_per_page=4' );
			if ($queryObject->have_posts()) {
			while ($queryObject->have_posts()) {
			?>
        <script type="text/javascript">
		$(document).ready(function() {
			$("#various<?php echo $post->ID;?>").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});
	</script>
        <?php
			$queryObject->the_post();
			?>
        <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
       <div class="news-onehome custom-video">
            <div class="video"> <a id="various<?php echo $post->ID;?>" href="#inline<?php echo $post->ID;?>"><?php /*?><img src="<?php echo $src[0];?>" alt="" style="width:100%;height:180px;" ><?php */?><?php echo do_shortcode('[types field="featured-video"]'); ?></a>
              <div style="display: none;">
                <div id="inline<?php echo $post->ID;?>" style="width:550px;height:400px;overflow:auto;padding:8px;border:5px solid #1C64AC;">
                  <table width="300" border="1" cellspacing="0" cellpadding="3" align="center">
                    <tr>
                      <td align="center"><strong>
                        <?php the_title();?>
                        </strong> </td>
                    </tr>
                    <tr>
                      <td><?php
$url = do_shortcode('[types field="add-video-link" class="" style=""][/types]'); 
parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
//echo $my_array_of_vars['v'];    
?>
                        <iframe width="548" height="374" src="http://www.youtube.com/embed/<?php echo $my_array_of_vars['v']; ?>"  frameborder="0" allowfullscreen></iframe>

                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <!--<span class="play-icon"></span>--> </div>
			  <div>
            <span> <a  class="custom-text" href="<?php the_permalink() ?>" style="color:#0f6eac;">
            <?php the_title();?>
            </a> </span>
			<h5 class="purple">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </h5> 

			</div>
            
            <!--<div>&nbsp;</div>-->
            <div class="responsive-cont">

              <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>

              </div>
            </div>
          </div>
        <?php
    }
}
?>
      </div>
    </div>
    <a class="more-video" href="<?php echo site_url()?>/video/" style="margin-top:20px;">More Videos</a> </div></section>



<section id="tv" class="news-section">
  <!-- video-section start -->
  <div class="container">
    <h2>TV</h2>
    <div class="news-wrap">
      <div class="news-slider">
        <?php
      	$queryObject = new WP_Query( 'post_type=Tv&posts_per_page=4' );
			if ($queryObject->have_posts()) {
			while ($queryObject->have_posts()) {
			?>
        <script type="text/javascript">
		$(document).ready(function() {
			$("#various<?php echo $post->ID;?>").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
		});
	</script>
        <?php
			$queryObject->the_post();
			?>
        <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
       <div class="news-onehome custom-video">
            <div class="video"> <a id="various<?php echo $post->ID;?>" href="#inline<?php echo $post->ID;?>"><?php /*?><img src="<?php echo $src[0];?>" alt="" style="width:100%;height:180px;" ><?php */?><?php echo do_shortcode('[types field="featured-video"]'); ?></a>
              <div style="display: none;">
                <div id="inline<?php echo $post->ID;?>" style="width:550px;height:400px;overflow:auto;padding:8px;border:5px solid #1C64AC;">
                  <table width="300" border="1" cellspacing="0" cellpadding="3" align="center">
                    <tr>
                      <td align="center"><strong>
                        <?php the_title();?>
                        </strong> </td>
                    </tr>
                    <tr>
                      <td><?php
$url = do_shortcode('[types field="add-video-link" class="" style=""][/types]'); 
parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
//echo $my_array_of_vars['v'];    
?>
                        <iframe width="548" height="374" src="http://www.youtube.com/embed/<?php echo $my_array_of_vars['v']; ?>"  frameborder="0" allowfullscreen></iframe>

                      </td>
                    </tr>
                  </table>
                </div>
              </div>
              <!--<span class="play-icon"></span>--> </div>
			  <div>
            <span> <a  class="custom-text" href="<?php the_permalink() ?>" style="color:#0f6eac;">
            <?php the_title();?>
            </a> </span>
			<h5 class="light-blue">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </h5>

			</div>
            
            <!--<div>&nbsp;</div>-->
            <div class="responsive-cont">

              <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>

              </div>
            </div>
          </div>
        <?php
    }
}
?>
      </div>
       <p>&nbsp;</p>
      <a class="more-video" href="<?php echo site_url()?>/all-tv/">More TV</a> </div>    
      
    </div>
    </div>
</section>



 <section id="radio" class="news-section radiohome">
<div class="container">
    <h2>radio</h2>
    <div class="news-wrap">
      <div class="news-slider">
      <?php
				$queryObject = new WP_Query( 'post_type=Radio&posts_per_page=12' );
				if ($queryObject->have_posts()) {
				while ($queryObject->have_posts()) {
				$queryObject->the_post();
				?>
        <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
        <div class="news-onehome"> <a href="<?php echo do_shortcode('[types field="add-radio-links" class="" style=""][/types]'); ?>"title="<?php the_title();?>" target="_blank"><?php the_post_thumbnail('home-video'); ?></a> <span><a class="custom-text" href="<?php the_permalink() ?>" style="color:#0f6eac;">
            <?php the_title();?>
            </a> </span>
			<h5 class="yellow">Posted by
            <?php the_author(); ?>
            |
            <?php the_time('M d, Y') ?>
          </h5>

            <div class="responsive-cont">

              <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>

              </div>
            </div>
          </div>
        <?php
    }
}
?>
      </div>
    </div>
    <br/>
    <div>
      <p>&nbsp;</p>
       <a class="more-video" href="<?php echo site_url()?>/more-radios/"  style="background-color:#ff0000;" >More Radio</a></div></div></section><section id="testimonial" class="testimonial-section"><!-- testimonial start -->
</section>

<?php get_footer(); ?>
<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<style type="text/css">
.maindiv{width:900px; margin:10px; padding:10px; margin:auto; overflow:hidden; -moz-border-radius:5px; border-radius:8px;}
.maindiv ul{list-style:none; margin:0px; padding:0px; color:fff;}
.maindiv ul li{background:#fff; border:1px solid #e5e5e5; border-radius:7px; box-shadow:2px 2px 2px 2px #888; float:left; font-size:12px; min-height:400px; list-style:none; margin:10px; padding:6px 0 6px; position:relative; text-align:left; width:246px;}
.video-title{color:#e85b30!important; display:block; font-size:20px; font-weight:300; line-height:22px; margin:46px 0 0; text-align:center; text-transform:uppercase;}
.news-one1{color:#ff0000; display:block; font-size:13px; font-weight:400; line-height:27px; margin:0; /* padding: 9px 3px 6px 0;*/ text-align:center; text-decoration:none; width:100%;}
</style>
<div class="innercontainer listing listing1">
<div class="language-plugin">
    <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
	</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      	  <style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div> 	
</div>
	<div class="padd">
	<?php if (have_posts()) : ?>    
    <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
    <?php /* If this is a category archive */ if (is_category()) { ?>
    <h2 class="pagetitle">Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2>
    <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
    <h2 class="pagetitle">Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>
    <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
    <h2 class="pagetitle">Archive for <?php the_time('F jS, Y'); ?></h2>
    <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
    <h2 class="pagetitle">Archive for <?php the_time('F, Y'); ?></h2>
    <?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
    <h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>
    <?php /* If this is an author archive */ } elseif (is_author()) { ?>
    <h2 class="pagetitle">Author Archive</h2>
    <?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
    <h2 class="pagetitle">Blog Archives</h2>
    <?php } ?>
    <div class="col-10"> 

   
    <h2 class="page-title">
	<?php
    printf( __( '%s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
	?></h2>
    <ul>
        <?php while (have_posts()) : the_post(); ?>
        <li <?php post_class('col-3 innercolpadd') ?>>
			<div class="news-one">
        <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail('home-video1'); ?>
        </a>
        <h4><a href="<?php the_permalink(); ?>" style="text-decoration:none;color: #0f6eac!important;">
        <?php the_title();?>
        </a> </h4>
    <div> 
    <span class="news-one1"> 
    By <?php the_author(); ?>|<?php the_time('M d, Y') ?>
    </span>
        <div class="custom-social">
            <div class="btn-group"> <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a> <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a> 
            </div>
        </div>
    </div>
</div>
		</li>   
        <?php endwhile; ?>
     </ul>   
        <div class="navigation">
            <div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
            <div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
        </div>
        <?php else :
        if ( is_category() ) { // If this is a category archive
            printf("<h2 class='center'>Sorry, but there aren't any posts in the %s category yet.</h2>", single_cat_title('',false));
        } else if ( is_date() ) { // If this is a date archive
            echo("<h2>Sorry, but there aren't any posts with this date.</h2>");
        } else if ( is_author() ) { // If this is a category archive
            $userdata = get_userdatabylogin(get_query_var('author_name'));
            printf("<h2 class='center'>Sorry, but there aren't any posts by %s yet.</h2>", $userdata->display_name);
        } else {
            echo("<h2 class='center'>No posts found.</h2>");
        }
        get_search_form();    
        endif;
        ?>
    </div><!--col8-->
    
    <div class="col-2" style="display:none;">
		<?php get_sidebar(); ?>
	</div><!--row-->
    </div><!--padd-->
</div><!--container-->
<?php get_footer(); ?>
<?php get_header();?>
<section id="testimonial" class="singlepost-section default-page">
  <!-- testimonial start -->
  
  <div class="container">
    <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div>  
    <div class="left-section">
   
      <h2 style="text-align:left;">
        <?php the_title();?>
      </h2>
      <!-- page content
    ================================================== -->
      <div id="body-right">
        <style>
		.link-pages {
			 clear:both;
		}
		</style>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h2></h2>
             
          <div class="entry-content">
            <?php if(have_posts()) : while(have_posts()) : the_post();?>
            <?php the_content(); ?>
            <?php wp_link_pages( 'before=<p class="link-pages">Page: ' );?>
            <div class="entry-meta">
              <?php twentythirteen_entry_meta(); ?>
              <?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
            </div>
            <!-- .entry-meta -->
            <?php endwhile; endif; ?>
          </div>
          <!-- .entry-content -->
        </div>
      </div>
      <!-- end page content
    ================================================== -->
    </div>
    <div class="right-section">
  
      <?php //get_sidebar();?>
    </div>
  </div>
</section>
<?php get_footer();?>

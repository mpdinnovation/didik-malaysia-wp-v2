<?php /*Template Name:More Radios*/?>
<?php get_header();?>

<style type="text/css">

.maindiv

	{ 

		width:900px; 

		margin:10px;

		padding:10px; 

		margin:auto;

		overflow:hidden;		

		-moz-border-radius:5px;

		border-radius:8px;

	}

.maindiv ul

	{ 

		list-style:none;

		margin:0px;

		padding:0px;

		color:fff;

	}

.maindiv ul li

	{ 

		 background: none repeat scroll 0 0 #fff;

    border: 1px solid #e5e5e5;

    border-radius: 7px;

    box-shadow: 2px 2px 2px 2px #888;

    float: left;

    font-size: 12px;

    min-height: 400px;

    list-style: outside none none;

    margin: 10px;

    padding: 6px 0 6px;

    position: relative;

    text-align: left;

    width: 246px;

	}

.video-title{

     color: #e85b30!important;

    display: block;

    font-size: 20px;

    font-weight: 300;

    line-height: 22px;

    margin: 46px 0 0;

    text-align: center;

    text-transform: uppercase;

}

.news-one1{

    color: #ff0000;

    display: block;

    font-size: 13px;

    font-weight: 400;

    line-height: 27px;

    margin: 0;

   /* padding: 9px 3px 6px 0;*/

    text-align: center;

    text-decoration: none;

    width: 100%;

}
.custom-social{position:absolute; bottom:20px; width:100%; text-align:center;}
</style>
<div class="innercontainer listing listing1 new-page radiopage">
<div class="language-plugin">
    <div class="glanguage">
      <div id="google_translate_element"></div>
      <script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ms,ta,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
	</script>
      <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
	  <style>
    div#google_translate_element div.goog-te-gadget-simple{background-color:white;}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span{color:grey}
    div#google_translate_element div.goog-te-gadget-simple a.goog-te-menu-value span:hover{color:#fff}
</style>
    </div> 	
</div>
 <div class="padd">
<div class="col-10" >

  <h2><?php the_title();?></h2>
  
  
<div class="filter-posts">
    <div id="tab-container" class="tab-container">
        <ul class='etabs'>
            <li class='tab'><a href="#tabs1-html">Latest</a></li>
            <li class='tab'><a href="#tabs1-js">Popular</a></li>
        </ul>
        
        <div id="tabs1-html">    
            <ul>
            <?php $loop = new WP_Query( array( 'post_type' => 'Radio', 'posts_per_page' => -1 ,'order'=> 'des') ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <li class="col-3 innercolpadd">
                    <div class="owl-item">
                        <div class="news-one">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home-video1'); ?></a>
                            <h4><a href="<?php the_permalink(); ?>" style="text-decoration:none;color: #0f6eac!important;"><?php the_title();?></a></h4>
                            <div> 
                                <span class="news-one1 yellow">By <?php the_author(); ?> | <?php the_time('M d, Y') ?></span>
                                <div class="custom-social">
                                    <div class="btn-group">
                                    <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a>
                                    <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endwhile; ?>
            <div style="clear:both;"></div>
            <div class="navigation1">
            <div class="alignleft"><?php next_posts_link('� Older Entries') ?></div>
            <div class="alignright"><?php previous_posts_link('Newer Entries �') ?></div>
            </div>
            <?php wp_reset_query(); ?>
            </ul>    
        </div><!--tab1-->
                
        <div id="tabs1-js">
            <?php /*?><?php dynamic_sidebar('news_popular'); ?><?php */?>
            
            <ul>
            <?php $loop = new WP_Query( array( 'post_type' => 'Radio', 'posts_per_page' => -1 ,'orderby'=> 'rand') ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
                <li class="col-3 innercolpadd">
                    <div class="owl-item">
                        <div class="news-one">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('home-video1'); ?></a>
                            <h4><a href="<?php the_permalink(); ?>" style="text-decoration:none;color: #0f6eac!important;"><?php the_title();?></a></h4>
                            <div> 
                                <span class="news-one1 yellow">By <?php the_author(); ?> | <?php the_time('M d, Y') ?></span>
                                <div class="custom-social">
                                    <div class="btn-group">
                                    <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a>
                                    <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endwhile; ?>
            <div style="clear:both;"></div>
            <div class="navigation1">
            <div class="alignleft"><?php next_posts_link('� Older Entries') ?></div>
            <div class="alignright"><?php previous_posts_link('Newer Entries �') ?></div>
            </div>
            <?php wp_reset_query(); ?>
            </ul>              
            
            
            
        </div><!--tab2-->
    </div><!--tab container-->
</div><!--filter posts-->  

</div>

<?php /*?><div class="col-2" >
<div>
  <h3 class="myh3class">Related Radios</h3>
</div>

<ul class="related-list">
    <?php $queryObject = new WP_Query( 'post_type=Radio&posts_per_page=5' ); if ($queryObject->have_posts()) { ?>
    <?php while ($queryObject->have_posts()) { $queryObject->the_post(); ?>
	<li>
		<?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" > <img src="<?php echo $src[0];?>" alt="" style="width:140px;height:100px;"> </a>
        <div style="clear:both;"></div>
		<a href="<?php the_permalink() ?>" class="latestnewsright" style="color:#666666 !important;font-size:12px;"  title="<?php the_title(); ?>"><?php the_title();?></a>
                <div class="related-social">
            <div class="btn-group">
            <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a>
            <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>
            </div>
        </div>

	</li>
    <?php } } ?>    
</ul>
</div><?php */?>



</div>
</div>
<?php get_footer(); ?>
<section id="testimonial" class="singlepost-section">
  <!-- testimonial start -->
  <div class="container">
    <div  style="width:72%;padding:4px;float:left;">
      <h2 style="text-align:left;">
        <?php the_title();?>
      </h2>
      <style type="text/css">
.maindiv
	{ 
		width:900px; 
		margin:10px;
		padding:10px; 
		margin:auto;
		overflow:hidden;		
		-moz-border-radius:5px;
		border-radius:8px;
	}
.maindiv ul
	{ 
		list-style:none;
		margin:0px;
		padding:0px;
		color:fff;
	}
.maindiv ul li
	{ 
		 background:#eaeaea;
    border: 1px solid #e5e5e5;
    border-radius: 7px;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    float: left;
    font-size: 12px;
    height: 230px;
    list-style: outside none none;
    margin: 10px;
    padding: 7px 0 4px;
    position: relative;
    text-align: left;
    width: 246px;
	}
.video-title{
     color: #e85b30!important;
    display: block;
    font-size: 20px;
    font-weight: 300;
    line-height: 22px;
    margin: 46px 0 0;
    text-align: center;
    text-transform: uppercase;
}
.playicon1{  background: url("../images/play.png") no-repeat scroll center center rgba(0, 0, 0, 0);
    cursor: pointer;
    height: 80px;
    left: 50%;
    margin-left: -40px;
    margin-top: -40px;
    position: absolute;
    top: 50%;
    width: 81px;}
</style>
      <div class="maindiv">
        <div align="center">
          <?php $loop = new WP_Query( array( 'post_type' => 'video', 'posts_per_page' => -1 ,'order'=> 'des') ); ?>
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
          <ul>
            <li>
              <div>
                <div style="border:0px solid red; margin:5px; float:left; padding:0px 10px 10px 10px;"><a href=" <?php echo do_shortcode('[types field="add-video-link" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>"><img src="<?php echo $src[0];?>" alt="" style="width:217px;height:174px;" ></a> </div>
              </div>
              <div class="video-title"> 
                <a href=" <?php echo do_shortcode('[types field="add-video-link" class="" style=""][/types]'); ?>" rel="wp-video-lightbox" title="<?php the_title();?>" style="border:0px solid red;text-decoration:none;color: #e85b30!important;">
				<?php the_title();?>
                </a> </div>
            </li>
          </ul>
          <?php endwhile; ?>
          <?php wp_reset_query(); ?>
        </div>
      </div>
    </div>
    <div style="width:25%;float:right;">
     <?php get_sidebar();?>
    </div>
  </div>
</section>
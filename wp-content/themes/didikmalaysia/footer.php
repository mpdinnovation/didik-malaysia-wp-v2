<div style="clear:both;"></div>
<footer class="footer">
<div class="go-up-btn"> <a href="#" class="scrollToTop"><img src="<?php echo get_template_directory_uri(); ?>/common/images/go-up.png" alt=""/></a> </div>
<div class="container"><div class="container" style="border-top:1px dashed #CCCCCC !important;padding-top:22px;" >
    <div class="social-icons">
        <ul>
        	<li><a href="https://www.facebook.com/didikmalaysia" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/common/images/f-2.png" alt="" /></a></li>
          <li><a href="https://twitter.com/didikmalaysia" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/common/images/f-1.png" alt="" /></a></li>
        </ul>
    </div>
</div>
<div class="footerinner">
  <!-- footer start -->
  <div class="container">
    <div class="footer-wrap">
	<?php wp_nav_menu( array( 'container' => '', 'theme_location' => 'footer' ) ); ?>
      <div class="footer-right"> 
      <a href="http://www.moe.gov.my/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/f-logo.png" alt="" /></a>
      </div>    
              
    </div>
  </div>
  </div>
</footer>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.0.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/common/js/jquery.bxslider.js"></script>
<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/common/js/owl.carousel.js"></script>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/common/js/jquery.meanmenu.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/jquery.easytabs.js"></script>
<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/common/js/browser_and_os_classes.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/fancybox.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/retina.min.js" type="text/javascript"></script>
<script  type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/common/js/custom.js"></script> 
<?php wp_footer();?>
</body>
</html>
<?php /*?><?php

if ( is_active_sidebar( 'sidebar-2' ) ) : ?>

	<div id="tertiary" class="sidebar-container" role="complementary">

		<div class="sidebar-inner">

			<div class="widget-area">

				<?php dynamic_sidebar( 'sidebar-2' ); ?>

			</div><!-- .widget-area -->

		</div><!-- .sidebar-inner -->

	</div><!-- #tertiary -->

<?php endif; ?><?php */?>

<div>
  <h3 class="myh3class">Related News</h3>
</div>

<ul class="related-list">
    <?php $queryObject = new WP_Query( 'post_type=News&posts_per_page=5' ); if ($queryObject->have_posts()) { ?>
    <?php while ($queryObject->have_posts()) { $queryObject->the_post(); ?>
	<li>
		<?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
		<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" > <img src="<?php echo $src[0];?>" alt="" style="width:140px;height:100px;"> </a>
        <div style="clear:both;"></div>
		<a href="<?php the_permalink() ?>" class="latestnewsright" style="color:#666666 !important;font-size:12px;"  title="<?php the_title(); ?>"><?php the_title();?></a>


        <div class="related-social">
            <div class="btn-group">
            <a class="btn btn-default btn-lg fb" target="_blank" title="On Facebook" href="http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>&amp;t=<?php the_title();?>"> <i class="fa fa-facebook fa-lg fb"></i> </a>
            <a class="btn btn-default btn-lg tw" target="_blank" title="On Twitter" href="http://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php the_title();?>"> <i class="fa fa-twitter fa-lg tw"></i> </a>
            </div>
        </div>
        
	</li>
    <?php } } ?>    
</ul>



<?php /*?><div>
  <table width="100%" border="0" cellspacing="2" cellpadding="2">
    <?php

$queryObject = new WP_Query( 'post_type=News&posts_per_page=5' );

if ($queryObject->have_posts()) {

    ?>
    <?php

while ($queryObject->have_posts()) {

        $queryObject->the_post();

        ?>
    <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
    <tr>
      <td><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" > <img src="<?php echo $src[0];?>" alt="" style="width:140px;height:100px;"> </a> </td>
    </tr>
    <tr>
      <td><a href="<?php the_permalink() ?>" class="latestnewsright" style="color:#666666 !important;font-size:12px;"  title="<?php the_title(); ?>" >
        <?php the_title();?>
        </a> </td>
    </tr>
    <tr>
      <td height="2" style="border-top:1px solid #EAEAEA;padding-bottom:6px;"></td>
    </tr>
    <?php

    }

	}

    ?>
  </table>
</div><?php */?>

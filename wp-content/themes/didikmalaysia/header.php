<!DOCTYPE html>
<html>
<head>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="google-translate-customization" content="444d908ab41c7d2c-04d2d06d36e40e66-g8f0a5b5b55aa1546-22">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/common/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/common/css/css3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/common/css/owl.carousel.css" />
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/common/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if( is_front_page()){ ?>
<header class="header home-header"><script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
$(window).on('load', function () {
 var $preloader = $('#page-preloader'),
$spinner = $preloader.find('.loader');
$spinner.fadeOut();
$preloader.delay(350).fadeOut('slow');
});
</script>
    <div class="header-middle">
        <ul class="banner-slider">
            <?php $loop = new WP_Query( array( 'post_type' => 'Slider', 'posts_per_page' => -1 ,'order'=> 'ASC') ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $src=wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');?>
            <li><img src="<?php echo $src[0];?>" alt="">
            	<div class="slide-text">
                <div class="innercontainer1">
                    <h3><a href="<?php echo do_shortcode('[types field="slide_link"]'); ?>" target="_blank"><?php echo do_shortcode('[types field="slide_text"]'); ?></a></h3>
                </div>
                </div>
            </li>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </ul>
<?php /*?>    <div class="featured-content">
    	<div class="innercontainer1">
        <h3>Welcome to Didik Malaysia 2.0</h3>
    </div>
    </div> <?php */?>      
        
    </div><!--header middle-->	
	<div class="header-top">
        <div class="header-container">	
            <div class="logo desktop-logo">
            	<h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/logo.png" alt=""/></a></h1>
            </div>
            <div class="search-wrap">
                <div class="search"> 
                	<a class="btn btn-default btn-lg tw mytwitterstyle" target="_blank" style="padding:3px;" title="On Twitter" href="https://twitter.com/didikmalaysia"> <i class="fa fa-twitter fa-lg tw"></i> </a> 
                </div>
            </div>                 
            <div class="nav">
                <div class="menu">
                <nav>
                    <a href="<?php echo home_url(); ?>" class="responsive-logo"><img src="<?php echo get_template_directory_uri(); ?>/common/images/logo.png" alt=""/>
                    </a>
                    
                    <?php wp_nav_menu( array( 'container' => '', 'theme_location' => 'primary' ) ); ?>
                    </nav>
                </div>        
            </div><!--nav-->           
        </div><!--header container-->
    </div><!--header top-->
    
    <div class="header-bottom">
        <ul>
            <li> <a href="#notifications"><img src="<?php echo get_template_directory_uri(); ?>/common/images/i8.png" alt=""/><span>notifications☉</span></a> </li>
            <li> <a href="#news"><img src="<?php echo get_template_directory_uri(); ?>/common/images/i1.png" alt=""/><span>news</span></a> </li>
            <li> <a href="#gallery"><img src="<?php echo get_template_directory_uri(); ?>/common/images/i9.png" alt=""/><span>Infographic</span></a> </li>
            <li> <a href="#video"><img src="<?php echo get_template_directory_uri(); ?>/common/images/i3.png" alt=""/><span>Videos</span></a> </li>
            <li> <a href="#tv"><img src="<?php echo get_template_directory_uri(); ?>/common/images/tv_icon.png" alt=""/><span>TV</span></a> </li>
            <li> <a href="<?php echo site_url()?>/#radio"><img src="<?php echo get_template_directory_uri(); ?>/common/images/i4.png" alt=""/><span>radio</span></a> </li>
            </ul>
    </div><!--header-bottom-->      
</header>
<?php }else{ ?>
<header class="header innerheader">
    <div class="header-top">
        <div class="header-container">
            <div class="logo">
                <h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/logo.png" alt=""/></a></h1>
            </div>
            <div class="nav">
                <div class="menu">
                    <nav>
                    <a href="<?php echo home_url(); ?>" class="responsive-logo"><img src="<?php echo get_template_directory_uri(); ?>/common/images/logo.png" alt=""/></a>
                    <?php wp_nav_menu( array( 'container' => '', 'theme_location' => 'inner' ) ); ?>
                    </nav>
                </div> <!--menu-->       
            </div><!--nav--> 
            <div class="search-wrap">
                <div class="search"> 
                    <a class="btn btn-default btn-lg tw mytwitterstyle" target="_blank" style="padding:3px;" title="On Twitter" href="https://twitter.com/didikmalaysia"> <i class="fa fa-twitter fa-lg tw"></i> </a> 
                </div>
            </div>
        </div><!--header container-->
    </div><!--headertop-->
</header>
<?php } ?>
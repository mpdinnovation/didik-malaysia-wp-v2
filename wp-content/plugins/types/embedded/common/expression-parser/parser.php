<?php
/**
*
*   Toolset Parser, advanced parser for arithmetic,logical and comparision expressions
*       by Nikos M. <nikos.m@icanlocalize.com>
*
*   Main Features:
*       + user variables
*       + support mathematical, and date functions
*       + limited date parsing
*
*   Additional Features:
*       + typed tokens
*       + typed user variables
*       + added string literals
*       + support advanced mathematical, string and date functions
*       + support advanced date operations (like (date + 3 days) or (3 days=date1-date2)) (not yet)
*       + support typecasting
*       + can parse and format every localized PHP date format
*       + precompilation of expressions into functions
*       + faster, optimized code
*       + use of closures for encapsulation and better access
*       + heavy refactoring, and various bug fixes
*
*   adapted to PHP form Toolset_Parser js version
*   inspired by JS Expression Evaluator by Prasad P. Khandekar
*
**/

class Toolset_Regex
{
    private $_regex='';

    public function Toolset_Regex($rx,$opts='')
    {
        // remove flags not supported by PHP
        $this->_regex='/'.$rx.'/'.str_replace('g','',$opts); // PHP does not support 'g' modifier
    }

    public function test($str)
    {
        return (preg_match($this->_regex,$str)==1);
    }
}

class Toolset_Functions
{
    private static $_cookies=null;
    private static $_regexs=array();
    private static $_params=array(
        'user'=>array(
            'ID'=>0,
            'role'=>'',
            'roles'=>array(),
            'login'=>'',
            'display_name'=>''
        )
    );

    public static function setParams($params)
    {
        self::$_params=array_merge(self::$_params,$params);
    }

    public static function Cookie($name)
    {
        if (!isset($_cookies)) $_cookies=&$_COOKIE;

        return (isset($_cookies[$name]))?$_cookies[$name]:'';
    }

    public static function User($att='')
    {
        $att=strtoupper($att);

        switch ($att)
        {
            case 'ID':
                return (string)(self::$_params['user']['ID'].'');
            case 'NAME':
                return self::$_params['user']['display_name'];
            case 'ROLE':
                return self::$_params['user']['role'];
            case 'LOGIN':
                return self::$_params['user']['login'];
            default:
                return '';
        }
        return '';
    }

    public static function Regex($rx, $opts='')  {return new Toolset_Regex($rx, $opts);}

    public static function Contains(&$a, $v)  {return in_array($v,$a);}
}

class Toolset_Date
{
    private $_timestamp;

 /*   getdate params
"seconds" 	Numeric representation of seconds 	0 to 59
"minutes" 	Numeric representation of minutes 	0 to 59
"hours" 	Numeric representation of hours 	0 to 23
"mday" 	Numeric representation of the day of the month 	1 to 31
"wday" 	Numeric representation of the day of the week 	0 (for Sunday) through 6 (for Saturday)
"mon" 	Numeric representation of a month 	1 through 12
"year" 	A full numeric representation of a year, 4 digits 	Examples: 1999 or 2003
"yday" 	Numeric representation of the day of the year 	0 through 365
"weekday" 	A full textual representation of the day of the week 	Sunday through Saturday
"month" A full textual representation of a month, such as January or March
*/
    private $_date=array(
        'hour'=>0,
        'min'=>0,
        'sec'=>0,
        'day_of_month'=>0,
        'day_of_week'=>0,
        'day_of_year'=>0,
        'day_of_week_string'=>'',
        'month_string'=>'',
        'month'=>0,
        'year'=>0
        );

    private static $_today=false;

    public static function setToday($date)
    {
        self::$_today=$date;
    }

    public static function getToday()
    {
        if (self::$_today)
            return new Toolset_Date(self::$_today);

        $today=new Toolset_Date();
        return  $today->setDateByTimestamp();
    }

    public function Toolset_Date($date=null)
    {
        if (isset($date))
            $this->setDate($date);
    }


    public function getDate($key=null)
    {
        if (!isset($key) || !in_array($key, array_keys($this->_date)))
            return $this->_date;
        else
            return $this->_date[$key];
    }

    public function setDate($date)
    {
        $hasYear=false;
        $hasMonth=false;
        $hasDay=false;

        foreach ($date as $k=>$v)
        {
            if ($k=='year')  $hasYear=true;
            if ($k=='month')  $hasMonth=true;
            if ($k=='day_of_year')  $hasDay=true;

            if (isset($this->_date[$k]))
                $this->_date[$k]=$v;
        }

        // fill all values
        if ($hasYear && $hasMonth && $hasDay)
            $this->setDateByTimestamp($this->getTimestamp());

        return $this;
    }

    public function getTimestamp()
    {
        if (class_exists("DateTime")) {
            $date = new DateTime("{$this->_date['year']}-{$this->_date['month']}-{$this->_date['day_of_month']} {$this->_date['hour']}:{$this->_date['min']}:{$this->_date['sec']}");
            return (method_exists('DateTime', 'getTimestamp')) ? $date->getTimestamp() : $date->format('U');
        } else
            return mktime ($this->_date['hour'], $this->_date['min'], $this->_date['sec'], $this->_date['month'], $this->_date['day_of_month'], $this->_date['year'] /*[, int $is_dst = -1 ]*/);
    }

    public function getNormalizedTimestamp()
    {
        if (class_exists("DateTime")) {
            $date = new DateTime("{$this->_date['year']}-{$this->_date['month']}-{$this->_date['day_of_month']}");
            return (method_exists('DateTime', 'getTimestamp')) ? $date->getTimestamp() : $date->format('U');
        } else
            return mktime (0, 0, 0, $this->_date['day_of_month'], $this->_date['month'], $this->_date['year'] /*[, int $is_dst = -1 ]*/);
    }

    public function setDateByTimestamp($time=null)
    {
        if (!isset($time))  $time=time();

        $dat=getdate($time);
        $date=array(
            'hour'=>0,
            'min'=>0,
            'sec'=>0,
            'month'=>$dat['mon'],
            'month_string'=>$dat['month'],
            'day_of_month'=>$dat['mday'],
            'day_of_week'=>$dat['wday'],
            'day_of_week_string'=>$dat['weekday'],
            'day_of_year'=>$dat['yday'],
            'year'=>$dat['year']
        );

        $this->_date=$date;

        return $this;
    }

    public function format($format)
    {
        //return date($format, $this->getTimestamp());

        // handle localized format
        return Toolset_DateParser::formatDate($this, $format);
    }
}

class Toolset_DateParser
{
    private static $_ZONE_NAMES = array('AM' => 'AM','PM' => 'PM');
    private static $_MONTH_NAMES = array('January','February','March','April','May','June','July','August','September','October','November','December');
    private static $_DAY_NAMES = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    private static $_ENGLISH_MONTH_NAMES = array('January','February','March','April','May','June','July','August','September','October','November','December');
    private static $_ENGLISH_DAY_NAMES = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

    private static function _to_int($str)
    {
        // return the integer representation of the string given as argument
        return intval($str);
    }

    private static function _escape_regexp($str)
    {
        // return string with special characters escaped
        //return preg_replace('#([\-\.\*\+\?\^\$\{\}\(\)\|\[\]\/\\])#', '\\$1', $str);
        return preg_quote($str, "/");
    }

    private static function _str_pad($n, $c)
    {
        if (strlen($n = $n . '') < $c)
        {
            return implode('0',array_fill((++$c) - strlen($n))). $n;
        }
        return $n;
    }

    private static function _is_string($s)
    {
        return is_string($s)?true:false;
    }

    public static function cmp($a, $b){ return $a['position'] - $b['position']; }

    public static function parseDate($date, $supposed_format)
    {
        // if already a date object
        if ($date instanceof Toolset_Date)
        {
            $date->setDate(array('hour'=>0,'min'=>0,'sec'=>0)); // normalize time part
            return $date;
        }

        if (
            !isset($date) ||
            !self::_is_string($date) ||
            !isset($supposed_format) ||
            !self::_is_string($supposed_format)
            )
        return false;

        // treat argument as a string
        $str_date = (string)$date . '';
        $supposed_format = (string)$supposed_format.'';

        // if value is given
        if ($str_date != '' && $supposed_format != '')
        {
                //echo '<br/>Date given<br />';
                // prepare the format by removing white space from it
                // and also escape characters that could have special meaning in a regular expression
                $format = self::_escape_regexp(preg_replace('/\s/','',$supposed_format));

                // allowed characters in date's format
                $format_chars = array('d','D','j','l','N','S','w','F','m','M','n','Y','y');

                // "matches" will contain the characters defining the date's format
                $matches = array();

                // "regexp" will contain the regular expression built for each of the characters used in the date's format
                $regexp = array();

            // iterate through the allowed characters in date's format
            for ($i = 0; $i < count($format_chars); $i++)
            {
                // if character is found in the date's format
                if (($position = strpos($format,$format_chars[$i])) > -1)

                    // save it, alongside the character's position
                    $matches[]=array('character'=> $format_chars[$i], 'position'=> $position);
            }

            // sort characters defining the date's format based on their position, ascending
            usort($matches,array('Toolset_DateParser','cmp'));

            // iterate through the characters defining the date's format
            for ($index=0; $index<count($matches); $index++)
            {
                $match=$matches[$index];

                // add to the array of regular expressions, based on the character
                switch ($match['character'])
                {

                    case 'd': $regexp[]='0[1-9]|[12][0-9]|3[01]'; break;
                    case 'D': $regexp[]='[a-z]{3}'; break;
                    case 'j': $regexp[]='[1-9]|[12][0-9]|3[01]'; break;
                    case 'l': $regexp[]='[a-z]+'; break;
                    case 'N': $regexp[]='[1-7]'; break;
                    case 'S': $regexp[]='st|nd|rd|th'; break;
                    case 'w': $regexp[]='[0-6]'; break;
                    case 'F': $regexp[]='[a-z]+'; break;
                    case 'm': $regexp[]='0[1-9]|1[012]+'; break;
                    case 'M': $regexp[]='[a-z]{3}'; break;
                    case 'n': $regexp[]='[1-9]|1[012]'; break;
                    case 'Y': $regexp[]='[0-9]{4}'; break;
                    case 'y': $regexp[]='[0-9]{2}'; break;

                }
            }

            // if we have an array of regular expressions
            if (!empty($regexp))
            {

                // we will replace characters in the date's format in reversed order
                $matches=array_reverse($matches);

                // iterate through the characters in date's format
                for ($index=0; $index<count($matches); $index++)
                {
                    $match=$matches[$index];

                    // replace each character with the appropriate regular expression
                    $format = str_replace($match['character'],'(' . $regexp[count($regexp) - $index - 1] . ')', $format);
                }

                // the final regular expression
                //$regexp = '/^' . $format . '$/ig';
                $regexp = '/^' . $format . '$/i';

                //echo '<br /><textarea>'.$regexp.'</textarea><br />';

                //preg_match_all('/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012]+)\/([0-9]{2})$/i','13/10/12',$foo);
                //print_r($foo);

                // if regular expression was matched
                if (preg_match_all($regexp, preg_replace('/\s/', '', $str_date),$segments))
                {
                //echo '<br/>Regex matched<br />';
                    //print_r($segments);

                    // check if date is a valid date (i.e. there's no February 31)
                    $english_days   = self::$_ENGLISH_DAY_NAMES;
                    $english_months = self::$_ENGLISH_MONTH_NAMES;

                    // by default, we assume the date is valid
                    $valid = true;

                    // reverse back the characters in the date's format
                    $matches=array_reverse($matches);

                    // iterate through the characters in the date's format
                    for ($index=0; $index<count($matches); $index++)
                    {
                        $match=$matches[$index];

                        // if the date is not valid, don't look further
                        if (!$valid) break; //return true;

                        // based on the character
                        switch ($match['character'])
                        {

                            case 'm':
                            case 'n':

                                // extract the month from the value entered by the user
                                $original_month = self::_to_int($segments[$index+1][0]);

                                break;

                            case 'd':
                            case 'j':

                                // extract the day from the value entered by the user
                                $original_day = self::_to_int($segments[$index+1][0]);

                                break;

                            case 'D':
                            case 'l':
                            case 'F':
                            case 'M':

                                // if day is given as day name, we'll check against the names in the used language
                                if ($match['character'] == 'D' || $match['character'] == 'l') $iterable = self::$_DAY_NAMES;

                                // if month is given as month name, we'll check against the names in the used language
                                else $iterable = self::$_MONTH_NAMES;

                                // by default, we assume the day or month was not entered correctly
                                $valid = false;

                                // iterate through the month/days in the used language
                                for ($key=0; $key<count($iterable); $key++)
                                {
                                    // if month/day was entered correctly, don't look further
                                    if ($valid) break; //return true;

                                    $value=$iterable[$key];

                                    // if month/day was entered correctly
                                    if (strtolower($segments[$index+1][0]) == strtolower(substr($value, 0, ($match['character'] == 'D' || $match['character'] == 'M' ? 3 : strlen($value)))))
                                    {

                                        // extract the day/month from the value entered by the user
                                        switch ($match['character'])
                                        {

                                            case 'D': $segments[$index+1][0] = substr($english_days[$key],0, 3); break;
                                            case 'l': $segments[$index+1][0] = $english_days[$key]; break;
                                            case 'F': $segments[$index+1][0] = $english_months[$key]; $original_month = $key + 1; break;
                                            case 'M': $segments[$index+1][0] = substr($english_months[$key],0, 3); $original_month = $key + 1; break;

                                        }

                                        // day/month value is valid
                                        $valid = true;

                                    }

                                }

                                break;

                            case 'Y':

                                // extract the year from the value entered by the user
                                $original_year = self::_to_int($segments[$index+1][0]);

                                break;

                            case 'y':

                                // extract the year from the value entered by the user
                                $original_year = '19' + self::_to_int($segments[$index+1][0]);

                                break;

                        }
                    }

                    // if everything is ok so far
                    if ($valid)
                    {
                        //echo '<br/>Date valid 1<br />';

                        // generate a Date object using the values entered by the user
                        // (handle also the case when original_month and/or original_day are undefined - i.e date format is "Y-m" or "Y")
                       /* print_r(array(
                            'year'=>$original_year,
                            'month'=>$original_month,
                            'day'=>$original_day,
                            ));*/

                        // if, after that, the date is the same as the date entered by the user
                        //if (date.getFullYear() == original_year && date.getDate() == (original_day || 1) && date.getMonth() == ((original_month || 1) - 1))
                        //var_dump(checkdate(10, 12, 2012));
                        if (checkdate ((int)$original_month, (int)$original_day, (int)$original_year))
                        {
                            // normalize time part, only date part checked
                            $date = new Toolset_Date(array(
                                'year'=>$original_year,
                                'month'=>$original_month,
                                'day_of_month'=>$original_day,
                                ));
                            $date->setDate(array('hour'=>0,'min'=>0,'sec'=>0));
                            // return the date as our date object
                        //echo '<br/>Date valid 2<br />';
                            return $date;
                        }
                    }
                }
            }
        }
        // if script gets this far, return false as something must've went wrong
        return false;
    }

    public static function formatDate($date, $format, $isTimestamp=false)
    {

        // if not a date object
        /*if (!isset($date) || !($date instanceof Toolset_Date))
        {
            return '';
        }
        $date->setDate(array('hour'=>0,'min'=>0,'sec'=>0)); // normalize time
        return $date->format($format);*/

        if ($isTimestamp)
        {
            $dat=new Toolset_Date();
            $date=$dat->setDateByTimestamp($date);
        }

        // if not a date object
        if (!isset($date) || !($date instanceof Toolset_Date))
        {
            return '';
        }

        $date->setDate(array('hour'=>0,'min'=>0,'sec'=>0)); // normalize time part

        $result = '';

        // extract parts of the date:
        // day number, 1 - 31
        $j = $date->getDate('day_of_month');

        // day of the week, 0 - 6, Sunday - Saturday
        $w = $date->getDate('day_of_week');

        // the name of the day of the week Sunday - Saturday
        $l = self::$_DAY_NAMES[$w];

        // the month number, 1 - 12
        $n = $date->getDate('month');// + 1;

        // the month name, January - December
        $f = self::$_MONTH_NAMES[$n - 1];

        // the year (as a string)
        $y = (string)$date->getDate('year') . '';

        // iterate through the characters in the format
        for ($i = 0; $i < strlen($format); $i++)
        {

            // extract the current character
            $chr = $format[$i];

            // see what character it is
            switch($chr)
            {
                // year as two digits
                case 'y': $y = substr($y,2);

                // year as four digits
                case 'Y': $result .= $y; break;

                // month number, prefixed with 0
                case 'm': $n = self::_str_pad($n, 2);

                // month number, not prefixed with 0
                case 'n': $result .= $n; break;

                // month name, three letters
                case 'M': $f = substr($f,0,3);

                // full month name
                case 'F': $result .= $f; break;

                // day number, prefixed with 0
                case 'd': $j = self::_str_pad($j, 2);

                // day number not prefixed with 0
                case 'j': $result .= $j; break;

                // day name, three letters
                case 'D': $l = substr($l, 0, 3);

                // full day name
                case 'l': $result .= $l; break;

                // ISO-8601 numeric representation of the day of the week, 1 - 7
                case 'N': $w++;

                // day of the week, 0 - 6
                case 'w': $result .= $w; break;

                // English ordinal suffix for the day of the month, 2 characters
                // (st, nd, rd or th (works well with j))
                case 'S':

                    if ($j % 10 == 1 && $j != '11') $result .= 'st';

                    else if ($j % 10 == 2 && $j != '12') $result .= 'nd';

                    else if ($j % 10 == 3 && $j != '13') $result .= 'rd';

                    else $result .= 'th';

                    break;

                // this is probably the separator
                default: $result .= $chr;

            }

        }
        // return formated date
        return $result;
    }

    public static function setDateLocaleStrings($dn=null, $mn=null, $zn=null)
    {
        if (isset($mn))
        {
            self::$_MONTH_NAMES = $mn;
        }
        if (isset($dn))
        {
            self::$_DAY_NAMES = $dn;
        }
        if (isset($zn))
            self::$_ZONE_NAMES = $zn;
    }

    public static function isDate($val, $format, &$getDate=null)
    {
        $date=self::parseDate($val,$format);
        if ($date!==false)
        {
            //echo '<hr />Date correct<hr />';
            if (isset($getDate))
            {
                $getDate['date']=$date;
            }
            return true;
        }
        return false;
    }

    public static function currentDate()
    {
        return Toolset_Date::getToday();
    }
}

class Toolset_Stack
{
    // Stack object constructor
    private $arrStack=array();
    private $intIndex=0;

    // Converts stack contents into a comma separated string
    public function toString()
    {
        $intCntr = 0;
        $strRet  =  "";
        if ($this->intIndex == 0) return null;
        for ($intCntr = 0; $intCntr < $this->intIndex; $intCntr++)
        {
            if ($strRet == '')
                $strRet .= print_r($this->arrStack[$intCntr]->val,true);
            else
                $strRet .= "," . print_r($this->arrStack[$intCntr]->val,true);
        }
        return $strRet;
    }

    // Returns size of stack
    public function Size()
    {
        return $this->intIndex;
    }

    // This method tells us if this Stack object is empty
    public function IsEmpty()
    {
        return ($this->intIndex == 0)?true:false;
    }

    // This method pushes a new element onto the top of the stack
    public function Push($newData)
    {
        $this->arrStack[$this->intIndex++] = $newData;
    }

    // This method pops the top element off of the stack